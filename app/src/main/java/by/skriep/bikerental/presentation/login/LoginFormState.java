package by.skriep.bikerental.presentation.login;

import androidx.annotation.Nullable;

public class LoginFormState {

    @Nullable
    private final Integer usernameError;
    @Nullable
    private final Integer passwordError;

    LoginFormState(@Nullable Integer usernameError, @Nullable Integer passwordError) {
        this.usernameError = usernameError;
        this.passwordError = passwordError;
    }

    @Nullable
    Integer getUsernameError() {
        return usernameError;
    }

    @Nullable
    Integer getPasswordError() {
        return passwordError;
    }

    boolean isDataValid() {
        return usernameError == null && passwordError == null;
    }
}
