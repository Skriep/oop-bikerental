package by.skriep.bikerental.presentation.map;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.yandex.mapkit.Animation;
import com.yandex.mapkit.MapKit;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.layers.ObjectEvent;
import com.yandex.mapkit.location.Location;
import com.yandex.mapkit.location.LocationListener;
import com.yandex.mapkit.location.LocationStatus;
import com.yandex.mapkit.map.CameraListener;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.IconStyle;
import com.yandex.mapkit.map.MapObjectCollection;
import com.yandex.mapkit.map.MapObjectTapListener;
import com.yandex.mapkit.map.PlacemarkMapObject;
import com.yandex.mapkit.mapview.MapView;
import com.yandex.mapkit.user_location.UserLocationLayer;
import com.yandex.mapkit.user_location.UserLocationObjectListener;
import com.yandex.mapkit.user_location.UserLocationView;
import com.yandex.runtime.image.ImageProvider;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;

public class MapFragment extends Fragment implements UserLocationObjectListener {

    private static final double DEFAULT_LATITUDE = 53.902284;
    private static final double DEFAULT_LONGITUDE = 27.561831;
    private static final float DEFAULT_ZOOM = 12.0f;

    private MapView mapView;
    private MapObjectCollection rentalPointsCollection;
    private UserLocationLayer userLocationLayer;
    private MapViewModel viewModel;
    private RentalPointClickListener rentalPointClickListener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity;
        if (context instanceof Activity) {
            activity = (Activity) context;
        } else {
            return;
        }
        if (!RentalPointClickListener.class.isAssignableFrom(context.getClass())) {
            throw new ClassCastException("Activity must implement RentalPointClickListener interface");
        }
        rentalPointClickListener = (RentalPointClickListener) context;

        BikeRentalApp app = (BikeRentalApp) activity.getApplication();
        viewModel = new ViewModelProvider(this, new MapViewModelFactory(app))
                .get(MapViewModel.class);
        viewModel.rentalPointsLoadResult.observe(this, rentalPointsLoadResult -> {
            if (rentalPointsCollection == null) {
                return;
            }
            if (rentalPointsLoadResult.getAddedPoints() == null) {
                return;
            }
            for (BikeRentalPoint rentalPoint: rentalPointsLoadResult.getAddedPoints()) {
                Point position = new Point(rentalPoint.getLatitude(), rentalPoint.getLongitude());
                PlacemarkMapObject mapObject = rentalPointsCollection.addPlacemark(
                        position,
                        ImageProvider.fromResource(context, R.drawable.bike_rental_location),
                        new IconStyle().setScale(0.12f).setAnchor(new PointF(0.5f, 1.0f)));
                mapObject.setUserData(rentalPoint);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    CameraListener cameraListener = (map, cameraPosition, cameraUpdateReason, b) -> {
        viewModel.onMapVisibleRegionChanged(map.getVisibleRegion());
    };

    MapObjectTapListener mapObjectTapListener = (mapObject, point) -> {
        Object userData = mapObject.getUserData();
        if (!(userData instanceof BikeRentalPoint)) {
            return false;
        }
        rentalPointClickListener.onRentalPointClicked((BikeRentalPoint) userData);
        return true;
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        MapKitFactory.initialize(view.getContext());

        mapView = view.findViewById(R.id.mapview);

        String mapStyle = "   [" +
                "       {" +
                "           \"types\": \"point\"," +
                "           \"tags\": {" +
                "               \"any\": [" +
                "                   \"poi\"," +
                "                   \"entrance\"" +
                "               ]" +
                "           }," +
                "           \"stylers\": {" +
                "               \"visibility\": \"off\"" +
                "           }" +
                "       }" +
                "   ]";
        mapView.getMap().setMapStyle(mapStyle);
        mapView.getMap().move(
                new CameraPosition(new Point(DEFAULT_LATITUDE, DEFAULT_LONGITUDE),
                        DEFAULT_ZOOM, 0.0f, 0.0f),
                new Animation(Animation.Type.SMOOTH, 0),
                null);

        requestLocationPermission();

        MapKit mapKit = MapKitFactory.getInstance();
        mapKit.resetLocationManagerToDefault();
        userLocationLayer = mapKit.createUserLocationLayer(mapView.getMapWindow());
        userLocationLayer.setVisible(true);
        userLocationLayer.setHeadingEnabled(false);
        userLocationLayer.setAutoZoomEnabled(false);
        userLocationLayer.setObjectListener(this);

        View myLocationButton = view.findViewById(R.id.buttonMyLocation);
        myLocationButton.setOnClickListener(v -> {
            requestLocationPermission();
            mapKit.createLocationManager().requestSingleUpdate(new LocationListener() {
                @Override
                public void onLocationUpdated(@NonNull Location location) {
                    mapView.getMap().move(
                            new CameraPosition(location.getPosition(),
                                    mapView.getMap().getCameraPosition().getZoom(),
                                    0.0f, 0.0f),
                            new Animation(Animation.Type.SMOOTH, 1), null);
                }

                @Override
                public void onLocationStatusUpdated(@NonNull LocationStatus locationStatus) { }
            });
            if (ContextCompat.checkSelfPermission(mapView.getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(mapView.getContext(), R.string.status_getting_location,
                        Toast.LENGTH_LONG).show();
            }
        });

        mapView.getMap().addCameraListener(cameraListener);

        rentalPointsCollection = mapView.getMap().getMapObjects().addCollection();
        rentalPointsCollection.addTapListener(mapObjectTapListener);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private final ActivityResultLauncher<String> locationPermissionResultLauncher = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            new ActivityResultCallback<Boolean>() {
                @Override
                public void onActivityResult(Boolean result) {
                    if(!result) {
                        Toast.makeText(mapView.getContext(), R.string.location_permission_required,
                                Toast.LENGTH_LONG).show();
                    }
                }
            });

    private void requestLocationPermission() {
        if (ContextCompat.checkSelfPermission(mapView.getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            locationPermissionResultLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        MapKitFactory.getInstance().onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
        MapKitFactory.getInstance().onStop();
    }

    @Override
    public void onObjectAdded(@NonNull UserLocationView userLocationView) {
        userLocationLayer.resetAnchor();

        userLocationView.getArrow().setIcon(
                ImageProvider.fromResource(mapView.getContext(), R.drawable.location_pin),
                new IconStyle().setScale(0.34f));

        userLocationView.getPin().setIcon(
                ImageProvider.fromResource(mapView.getContext(), R.drawable.location_pin),
                new IconStyle().setScale(0.34f));

        userLocationView.getAccuracyCircle().setFillColor(
                Color.argb(0x55, 0x00, 0xAA, 0x00));
    }

    @Override
    public void onObjectRemoved(@NonNull UserLocationView userLocationView) {

    }

    @Override
    public void onObjectUpdated(@NonNull UserLocationView userLocationView, @NonNull ObjectEvent objectEvent) {

    }

    public interface RentalPointClickListener {
        void onRentalPointClicked(BikeRentalPoint rentalPoint);
    }
}