package by.skriep.bikerental.presentation.map;

import androidx.annotation.Nullable;

import java.util.List;

import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;

public class RentalPointsLoadResult {

    @Nullable
    private final List<BikeRentalPoint> addedPoints;
    @Nullable
    private final List<BikeRentalPoint> removedPoints;

    RentalPointsLoadResult(@Nullable List<BikeRentalPoint> addedPoints,
                           @Nullable List<BikeRentalPoint> removedPoints) {
        this.addedPoints = addedPoints;
        this.removedPoints = removedPoints;
    }

    @Nullable
    public List<BikeRentalPoint> getAddedPoints() {
        return addedPoints;
    }

    @Nullable
    public List<BikeRentalPoint> getRemovedPoints() {
        return removedPoints;
    }
}
