package by.skriep.bikerental.presentation.login;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.users.User;
import by.skriep.bikerental.domain.exceptions.auth.LoginException;
import by.skriep.bikerental.domain.usecases.auth.LoginByUsernameUseCase;

public class LoginViewModel extends ViewModel {

    private final LoginByUsernameUseCase loginByUsernameUseCase;
    private final MutableLiveData<LoginFormState> loginFormStateMutable = new MutableLiveData<>();
    private final MutableLiveData<LoginResult> loginResultMutable = new MutableLiveData<>();

    public final LiveData<LoginFormState> loginFormState = loginFormStateMutable;
    public final LiveData<LoginResult> loginResult = loginResultMutable;

    public LoginViewModel(@NonNull LoginByUsernameUseCase loginByUsernameUseCase) {
        this.loginByUsernameUseCase = loginByUsernameUseCase;
    }

    public void login(String username, String password) {
        try {
            User user = loginByUsernameUseCase.execute(username, password);
            loginResultMutable.setValue(new LoginResult(user));
        } catch (LoginException e) {
            loginResultMutable.setValue(new LoginResult(R.string.invalid_credentials_error));
        }
    }

    public void onLoginDataChanged(String username, String password) {
        Integer usernameError = null;
        Integer passwordError = null;
        if (!isUsernameValid(username)) {
            usernameError = R.string.invalid_username_error;
        }
        if (!isPasswordValid(password)) {
            passwordError = R.string.empty_field_error;
        }
        loginFormStateMutable.setValue(new LoginFormState(usernameError, passwordError));
    }

    private boolean isUsernameValid(String username) {
        if (username == null) {
            return false;
        }
        return !username.trim().isEmpty();
    }

    private boolean isPasswordValid(String password) {
        return password != null && !password.isEmpty();
    }
}
