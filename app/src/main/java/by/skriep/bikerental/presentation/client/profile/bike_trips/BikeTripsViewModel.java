package by.skriep.bikerental.presentation.client.profile.bike_trips;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.trips.BikeTrip;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.exceptions.bikes.BicycleBookedByAnotherClientException;
import by.skriep.bikerental.domain.exceptions.bikes.booking.BicycleRentedException;
import by.skriep.bikerental.domain.exceptions.bikes.renting.BicycleAlreadyRentedException;
import by.skriep.bikerental.domain.exceptions.bikes.renting.BicycleBookedException;
import by.skriep.bikerental.domain.exceptions.bikes.renting.RentingException;
import by.skriep.bikerental.domain.usecases.bikes.CancelBicycleBookingUseCase;
import by.skriep.bikerental.domain.usecases.bikes.RentBicycleUseCase;
import by.skriep.bikerental.domain.usecases.client.GetBookedByClientBikesUseCase;
import by.skriep.bikerental.domain.usecases.client.GetClientBikeTripsUseCase;
import by.skriep.bikerental.presentation.client.rental_point.BicycleOperationResult;

public class BikeTripsViewModel extends ViewModel {

    public final LiveData<List<BikeTrip>> bikeTrips;

    public BikeTripsViewModel(@NonNull GetClientBikeTripsUseCase getClientBikeTripsUseCase,
                              @NonNull Client client) {
        this.bikeTrips = getClientBikeTripsUseCase.execute(client);
    }
}
