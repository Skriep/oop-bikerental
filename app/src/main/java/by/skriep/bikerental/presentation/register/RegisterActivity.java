package by.skriep.bikerental.presentation.register;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.R;

public class RegisterActivity extends AppCompatActivity {

    boolean nameTextChanged = false;
    boolean surnameTextChanged = false;
    boolean patronymicTextChanged = false;
    boolean usernameTextChanged = false;
    boolean passwordTextChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        BikeRentalApp app = (BikeRentalApp) getApplication();
        RegisterViewModel viewModel =
                new ViewModelProvider(this, new RegisterViewModelFactory(app))
                        .get(RegisterViewModel.class);

        EditText nameEditText = findViewById(R.id.editTextName);
        EditText surnameEditText = findViewById(R.id.editTextSurname);
        EditText patronymicEditText = findViewById(R.id.editTextPatronymic);
        EditText usernameEditText = findViewById(R.id.editTextUsername);
        EditText passwordEditText = findViewById(R.id.editTextPassword);
        EditText passwordRepeatedEditText = findViewById(R.id.editTextPasswordRepeated);
        View registerButton = findViewById(R.id.buttonRegister);
        View goToLoginButton = findViewById(R.id.textViewLogin);

        viewModel.registerFormState.observe(this, registerFormState -> {
            if (registerFormState == null) {
                return;
            }
            registerButton.setEnabled(registerFormState.isDataValid());
            Integer nameError = registerFormState.getNameError(),
                    surnameError = registerFormState.getSurnameError(),
                    patronymicError = registerFormState.getPatronymicError(),
                    usernameError = registerFormState.getUsernameError(),
                    passwordError = registerFormState.getPasswordError(),
                    passwordRepeatedError = registerFormState.getPasswordRepeatedError();
            if (nameError != null && nameTextChanged) {
                nameEditText.setError(getString(nameError));
            }
            if (surnameError != null && surnameTextChanged) {
                surnameEditText.setError(getString(surnameError));
            }
            if (patronymicError != null && patronymicTextChanged) {
                patronymicEditText.setError(getString(patronymicError));
            }
            if (usernameError != null && usernameTextChanged) {
                usernameEditText.setError(getString(usernameError));
            }
            if (passwordError != null && passwordTextChanged) {
                passwordEditText.setError(getString(passwordError));
            }
            if (passwordRepeatedError != null) {
                passwordRepeatedEditText.setError(getString(passwordRepeatedError));
            } else {
                passwordRepeatedEditText.setError(null);
            }
        });

        viewModel.registerResult.observe(this, registerResult -> {
            if (registerResult == null) {
                return;
            }
            if (registerResult instanceof RegisterResult.Error) {
                Toast.makeText(this, ((RegisterResult.Error) registerResult).getError(),
                        Toast.LENGTH_LONG).show();
                return;
            }
            if (registerResult instanceof RegisterResult.Success) {
                Toast.makeText(this, R.string.successful_registration,
                        Toast.LENGTH_LONG).show();
                setResult(Activity.RESULT_OK);
                finish();
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (!nameTextChanged && s == nameEditText.getEditableText()) {
                    nameTextChanged = true;
                }
                if (!surnameTextChanged && s == surnameEditText.getEditableText()) {
                    surnameTextChanged = true;
                }
                if (!patronymicTextChanged && s == patronymicEditText.getEditableText()) {
                    patronymicTextChanged = true;
                }
                if (!usernameTextChanged && s == usernameEditText.getEditableText()) {
                    usernameTextChanged = true;
                }
                if (!passwordTextChanged && s == passwordEditText.getEditableText()) {
                    passwordTextChanged = true;
                }
                viewModel.onRegisterDataChanged(
                        nameEditText.getText().toString(),
                        surnameEditText.getText().toString(),
                        patronymicEditText.getText().toString(),
                        usernameEditText.getText().toString(),
                        passwordEditText.getText().toString(),
                        passwordRepeatedEditText.getText().toString());
            }
        };
        nameEditText.addTextChangedListener(afterTextChangedListener);
        surnameEditText.addTextChangedListener(afterTextChangedListener);
        patronymicEditText.addTextChangedListener(afterTextChangedListener);
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordRepeatedEditText.addTextChangedListener(afterTextChangedListener);

        registerButton.setOnClickListener(view -> viewModel.register(
                nameEditText.getText().toString(),
                surnameEditText.getText().toString(),
                patronymicEditText.getText().toString(),
                usernameEditText.getText().toString(),
                passwordEditText.getText().toString()));

        goToLoginButton.setOnClickListener(view -> finish());
    }
}