package by.skriep.bikerental.presentation.register;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.domain.usecases.auth.RegisterClientByUsernameUseCase;

public class RegisterViewModelFactory implements ViewModelProvider.Factory {

    private final BikeRentalApp app;

    public RegisterViewModelFactory(BikeRentalApp app) {
        this.app = app;
    }

    @Override
    @NonNull
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(RegisterViewModel.class)) {
            return (T) new RegisterViewModel(
                    new RegisterClientByUsernameUseCase(app.getRepository()));
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
