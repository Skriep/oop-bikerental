package by.skriep.bikerental.presentation.register;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import by.skriep.bikerental.domain.entities.users.User;

public class RegisterResult {

    private RegisterResult() {  }

    public static final class Success extends RegisterResult {
        private final User registeredUser;

        public Success(@NonNull User registeredUser) {
            this.registeredUser = registeredUser;
        }

        public User getRegisteredUser() {
            return this.registeredUser;
        }
    }

    public static final class Error extends RegisterResult {
        @StringRes
        private final Integer error;

        public Error(@NonNull Integer error) {
            this.error = error;
        }

        public Integer getError() {
            return this.error;
        }
    }
}
