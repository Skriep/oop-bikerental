package by.skriep.bikerental.presentation.client.rental_point;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import by.skriep.bikerental.domain.entities.bikes.Bicycle;

public abstract class BicycleOperationResult {

    @NonNull
    public abstract Bicycle getBicycle();

    @StringRes
    public abstract Integer getMessage();

    public static final class Success extends BicycleOperationResult {
        @StringRes
        private final Integer success;
        private final Bicycle bicycle;

        public Success(Bicycle bicycle, @NonNull Integer success) {
            this.bicycle = bicycle;
            this.success = success;
        }

        @Override
        public Integer getMessage() {
            return this.success;
        }

        @NonNull
        @Override
        public Bicycle getBicycle() {
            return bicycle;
        }
    }

    public static final class Error extends BicycleOperationResult {
        @StringRes
        private final Integer error;
        private final Bicycle bicycle;

        public Error(Bicycle bicycle, @NonNull Integer error) {
            this.bicycle = bicycle;
            this.error = error;
        }

        @Override
        public Integer getMessage() {
            return this.error;
        }

        @NonNull
        @Override
        public Bicycle getBicycle() {
            return bicycle;
        }
    }
}