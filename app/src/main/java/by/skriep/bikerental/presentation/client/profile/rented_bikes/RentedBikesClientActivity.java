package by.skriep.bikerental.presentation.client.profile.rented_bikes;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.entities.users.User;

public class RentedBikesClientActivity extends AppCompatActivity {

    private RentedBikesViewModel viewModel;
    private BikeRentalApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rented_bikes_client);

        app = (BikeRentalApp) getApplication();

        User loggedInUser = app.getLoggedInUser();
        if (!(loggedInUser instanceof Client)) {
            Toast.makeText(this, R.string.not_a_client_error, Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        Client client = (Client) loggedInUser;

        RecyclerView recyclerView = findViewById(R.id.recyclerViewBikes);
        View emptyView = findViewById(R.id.textViewEmpty);

        BikesAdapter adapter = new BikesAdapter(bicycle -> viewModel.finishRentingBicycle(client, bicycle));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        viewModel = new ViewModelProvider(this, new RentedBikesViewModelFactory(app, client))
                .get(RentedBikesViewModel.class);

        viewModel.bicycles.observe(this, bicycles -> {
            adapter.setBicycles(bicycles);
            if (bicycles.isEmpty()) {
                emptyView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            } else {
                emptyView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        });

        viewModel.bicycleRentingFinishResult.observe(this, bicycleBookingCancelResult ->
                Toast.makeText(this, bicycleBookingCancelResult.getMessage(), Toast.LENGTH_LONG).show());
    }
}