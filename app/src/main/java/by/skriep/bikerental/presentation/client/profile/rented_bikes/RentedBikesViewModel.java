package by.skriep.bikerental.presentation.client.profile.rented_bikes;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.exceptions.bikes.BicycleNotRentedException;
import by.skriep.bikerental.domain.exceptions.bikes.BicycleRentedByAnotherClientException;
import by.skriep.bikerental.domain.usecases.bikes.FinishBicycleRentingUseCase;
import by.skriep.bikerental.domain.usecases.client.GetRentedByClientBikesUseCase;
import by.skriep.bikerental.presentation.client.rental_point.BicycleOperationResult;

public class RentedBikesViewModel extends ViewModel {

    private final FinishBicycleRentingUseCase finishBicycleRentingUseCase;
    private final MutableLiveData<BicycleOperationResult> bicycleRentingFinishResultMutable = new MutableLiveData<>();

    public final LiveData<List<Bicycle>> bicycles;
    public final LiveData<BicycleOperationResult> bicycleRentingFinishResult = bicycleRentingFinishResultMutable;

    public RentedBikesViewModel(@NonNull GetRentedByClientBikesUseCase getRentedByClientBikesUseCase,
                                @NonNull FinishBicycleRentingUseCase finishBicycleRentingUseCase,
                                @NonNull Client client) {
        this.finishBicycleRentingUseCase = finishBicycleRentingUseCase;
        this.bicycles = getRentedByClientBikesUseCase.execute(client);
    }

    public void finishRentingBicycle(Client client, Bicycle bicycle) {
        try {
            finishBicycleRentingUseCase.execute(client, bicycle);
            bicycleRentingFinishResultMutable.setValue(new BicycleOperationResult.Success(bicycle, R.string.successfully_finished_renting));
        } catch (BicycleNotRentedException e) {
            bicycleRentingFinishResultMutable.setValue(new BicycleOperationResult.Error(bicycle, R.string.bicycle_not_rented_error));
        } catch (BicycleRentedByAnotherClientException e) {
            bicycleRentingFinishResultMutable.setValue(new BicycleOperationResult.Error(bicycle, R.string.bicycle_rented_by_others_error));
        }
    }
}
