package by.skriep.bikerental.presentation.client.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;
import by.skriep.bikerental.presentation.client.profile.ProfileClientActivity;
import by.skriep.bikerental.presentation.client.rental_point.BikeRentalPointClientActivity;
import by.skriep.bikerental.presentation.map.MapFragment;

public class MainClientActivity extends AppCompatActivity implements MapFragment.RentalPointClickListener {

    private static final int MENU_ITEM_PROFILE_ID = 0;

    private BikeRentalApp app;

    ActivityResultLauncher<Intent> profileActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == RESULT_FIRST_USER) {
                    finish();
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        app = (BikeRentalApp) getApplication();

        findViewById(R.id.buttonPlus).setVisibility(View.INVISIBLE);

        BottomNavigationView navigationView = findViewById(R.id.bottomNavigationView);
        Menu navigationMenu = navigationView.getMenu();
        MenuItem menuItemProfile = navigationMenu
                .add(0, MENU_ITEM_PROFILE_ID, Menu.NONE, R.string.profile_menu_item)
                .setIcon(R.drawable.ic_person_24);
        menuItemProfile.setOnMenuItemClickListener(item -> {
            Intent intent = new Intent(this, ProfileClientActivity.class);
            profileActivityLauncher.launch(intent);
            return true;
        });
    }

    @Override
    public void onRentalPointClicked(BikeRentalPoint rentalPoint) {
        app.setSelectedRentalPoint(rentalPoint);
        Intent intent = new Intent(this, BikeRentalPointClientActivity.class);
        startActivity(intent);
    }
}