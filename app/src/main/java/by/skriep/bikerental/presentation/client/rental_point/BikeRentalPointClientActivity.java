package by.skriep.bikerental.presentation.client.rental_point;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.entities.users.User;

public class BikeRentalPointClientActivity extends AppCompatActivity {

    private BikeRentalPointViewModel viewModel;
    private BikeRentalApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bike_rental_point_client);

        app = (BikeRentalApp) getApplication();

        User loggedInUser = app.getLoggedInUser();
        if (!(loggedInUser instanceof Client)) {
            Toast.makeText(this, R.string.not_a_client_error, Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        Client client = (Client) loggedInUser;

        RecyclerView recyclerView = findViewById(R.id.recyclerViewBikes);
        View emptyView = findViewById(R.id.textViewEmpty);

        BikesAdapter adapter = new BikesAdapter(new BikeClickListener() {
            @Override
            public void onBook(Bicycle bicycle) {
                viewModel.bookBicycle(client, bicycle);
            }

            @Override
            public void onRent(Bicycle bicycle) {
                viewModel.rentBicycle(client, bicycle);
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        viewModel = new ViewModelProvider(this, new BikeRentalPointViewModelFactory(app, app.getSelectedRentalPoint()))
                .get(BikeRentalPointViewModel.class);

        viewModel.bicycles.observe(this, bicycles -> {
            adapter.setBicycles(bicycles);
            if (bicycles.isEmpty()) {
                emptyView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            } else {
                emptyView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        });

        viewModel.bicycleBookingResult.observe(this, bicycleBookingResult ->
                Toast.makeText(this, bicycleBookingResult.getMessage(), Toast.LENGTH_LONG).show());

        viewModel.bicycleRentingResult.observe(this, bicycleRentingResult -> {
            Toast.makeText(this, bicycleRentingResult.getMessage(), Toast.LENGTH_LONG).show();
            if (bicycleRentingResult instanceof BicycleOperationResult.Success) {
                Snackbar.make(recyclerView,
                        String.format("%s: %s",
                                getString(R.string.bike_lock_code),
                                bicycleRentingResult.getBicycle().getBikeLock().getLockCode()),
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.ok, v -> {})
                        .show();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        app.setSelectedRentalPoint(null);
    }
}