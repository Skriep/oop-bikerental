package by.skriep.bikerental.presentation.client.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.entities.users.User;
import by.skriep.bikerental.domain.exceptions.auth.InvalidPasswordException;
import by.skriep.bikerental.domain.usecases.client.ChangeUserPasswordUseCase;

public class PasswordChangeClientActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_change_client);

        BikeRentalApp app = (BikeRentalApp) getApplication();

        User loggedInUser = app.getLoggedInUser();
        if (!(loggedInUser instanceof Client)){
            Toast.makeText(this, R.string.not_a_client_error, Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        Client client = (Client) loggedInUser;

        EditText oldPasswordEditText = findViewById(R.id.editTextOldPassword);
        EditText passwordEditText = findViewById(R.id.editTextNewPassword);
        EditText passwordRepeatedEditText = findViewById(R.id.editTextNewPasswordRepeated);
        View buttonCancel = findViewById(R.id.buttonCancel);
        View buttonSave = findViewById(R.id.buttonSave);

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                boolean error = false;
                if (passwordEditText.getText().toString().length() < 5) {
                    passwordEditText.setError(getString(R.string.password_too_short_error));
                    error = true;
                }
                if (!passwordEditText.getText().toString().equals(
                        passwordRepeatedEditText.getText().toString())) {
                    passwordRepeatedEditText.setError(getString(R.string.passwords_do_not_match_error));
                    error = true;
                } else {
                    passwordRepeatedEditText.setError(null);
                }
                buttonSave.setEnabled(!error);
            }
        };
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordRepeatedEditText.addTextChangedListener(afterTextChangedListener);

        buttonCancel.setOnClickListener(view -> finish());

        ChangeUserPasswordUseCase changeUserPasswordUseCase =
                new ChangeUserPasswordUseCase(app.getRepository());

        buttonSave.setOnClickListener(view -> {
            try {
                changeUserPasswordUseCase.execute(client,
                        oldPasswordEditText.getText().toString(),
                        passwordEditText.getText().toString());
            } catch (InvalidPasswordException e) {
                Toast.makeText(this, R.string.invalid_old_password_error, Toast.LENGTH_LONG).show();
                return;
            }
            Toast.makeText(this, R.string.data_saved, Toast.LENGTH_LONG).show();
            finish();
        });
    }
}