package by.skriep.bikerental.presentation.login;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.domain.usecases.auth.LoginByUsernameUseCase;

public class LoginViewModelFactory implements ViewModelProvider.Factory {

    private final BikeRentalApp app;

    public LoginViewModelFactory(BikeRentalApp app) {
        this.app = app;
    }

    @Override
    @NonNull
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new LoginViewModel(new LoginByUsernameUseCase(app.getRepository()));
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}