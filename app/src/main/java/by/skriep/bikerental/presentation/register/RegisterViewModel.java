package by.skriep.bikerental.presentation.register;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.exceptions.auth.UsernameAlreadyRegisteredException;
import by.skriep.bikerental.domain.usecases.auth.RegisterClientByUsernameUseCase;

public class RegisterViewModel extends ViewModel {

    private final RegisterClientByUsernameUseCase registerClientByUsernameUseCase;
    private final MutableLiveData<RegisterFormState> registerFormStateMutable = new MutableLiveData<>();
    private final MutableLiveData<RegisterResult> registerResultMutable = new MutableLiveData<>();

    public final LiveData<RegisterFormState> registerFormState = registerFormStateMutable;
    public final LiveData<RegisterResult> registerResult = registerResultMutable;

    public RegisterViewModel(RegisterClientByUsernameUseCase registerClientByUsernameUseCase) {
        this.registerClientByUsernameUseCase = registerClientByUsernameUseCase;
    }

    public void register(String name, String surname, String patronymic,
                         String username, String password) {
        try {
            Client registeredClient = registerClientByUsernameUseCase.execute(name, surname,
                    patronymic, username, password);
            registerResultMutable.setValue(new RegisterResult.Success(registeredClient));
        } catch (UsernameAlreadyRegisteredException e) {
            registerResultMutable.setValue(
                    new RegisterResult.Error(R.string.username_already_registered_error));
        }
    }

    public void onRegisterDataChanged(String name, String surname, String patronymic,
                                      String username, String password, String passwordRepeated) {
        Integer nameError = null, surnameError = null, patronymicError = null,
                usernameError = null, passwordError = null, passwordRepeatedError = null;
        if (isStringEmpty(name)) {
            nameError = R.string.empty_field_error;
        }
        if (isStringEmpty(surname)) {
            surnameError = R.string.empty_field_error;
        }
        if (isStringEmpty(patronymic)) {
            patronymicError = R.string.empty_field_error;
        }
        if (!isUsernameValid(username)) {
            usernameError = R.string.invalid_username_error;
        }
        if (!isPasswordValid(password)) {
            passwordError = R.string.password_too_short_error;
        }
        if (passwordRepeated == null || !passwordRepeated.equals(password)) {
            passwordRepeatedError = R.string.passwords_do_not_match_error;
        }
        registerFormStateMutable.setValue(new RegisterFormState(nameError, surnameError,
                patronymicError, usernameError, passwordError, passwordRepeatedError));
    }

    private boolean isStringEmpty(String string) {
        return string != null && string.trim().isEmpty();
    }

    private boolean isUsernameValid(String username) {
        if (username == null) {
            return false;
        }
        if (isStringEmpty(username)) {
            return false;
        }
        return username.matches("^[A-Za-z0-9_]+$");
    }

    private boolean isPasswordValid(String password) {
        if (password == null) {
            return false;
        }
        if (isStringEmpty(password)) {
            return false;
        }
        return password.length() >= 5;
    }
}
