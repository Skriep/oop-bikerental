package by.skriep.bikerental.presentation.client.profile.rented_bikes;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.data.repositories.BikeRentalRepository;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.usecases.bikes.FinishBicycleRentingUseCase;
import by.skriep.bikerental.domain.usecases.client.GetRentedByClientBikesUseCase;

public class RentedBikesViewModelFactory implements ViewModelProvider.Factory {

    private final BikeRentalApp app;
    private final Client client;

    public RentedBikesViewModelFactory(BikeRentalApp app, Client client) {
        this.app = app;
        this.client = client;
    }

    @Override
    @NonNull
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(RentedBikesViewModel.class)) {
            BikeRentalRepository repository = app.getRepository();
            return (T) new RentedBikesViewModel(
                    new GetRentedByClientBikesUseCase(repository),
                    new FinishBicycleRentingUseCase(repository, repository),
                    client);
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}