package by.skriep.bikerental.presentation.client.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.entities.users.User;
import by.skriep.bikerental.domain.usecases.client.UpdateFullClientNameUseCase;

public class PersonalInfoClientActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info_client);

        BikeRentalApp app = (BikeRentalApp) getApplication();

        User loggedInUser = app.getLoggedInUser();
        if (!(loggedInUser instanceof Client)){
            Toast.makeText(this, R.string.not_a_client_error, Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        Client client = (Client) loggedInUser;

        EditText nameEditText = findViewById(R.id.editTextName);
        EditText surnameEditText = findViewById(R.id.editTextSurname);
        EditText patronymicEditText = findViewById(R.id.editTextPatronymic);
        View buttonCancel = findViewById(R.id.buttonCancel);
        View buttonSave = findViewById(R.id.buttonSave);

        nameEditText.setText(client.getName());
        surnameEditText.setText(client.getSurname());
        patronymicEditText.setText(client.getPatronymic());

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                boolean error = false;
                if (nameEditText.getText().toString().isEmpty()) {
                    nameEditText.setError(getString(R.string.empty_field_error));
                    error = true;
                }
                if (surnameEditText.getText().toString().isEmpty()) {
                    surnameEditText.setError(getString(R.string.empty_field_error));
                    error = true;
                }
                if (patronymicEditText.getText().toString().isEmpty()) {
                    patronymicEditText.setError(getString(R.string.empty_field_error));
                    error = true;
                }
                buttonSave.setEnabled(!error);
            }
        };
        nameEditText.addTextChangedListener(afterTextChangedListener);
        surnameEditText.addTextChangedListener(afterTextChangedListener);
        patronymicEditText.addTextChangedListener(afterTextChangedListener);

        buttonCancel.setOnClickListener(view -> finish());

        UpdateFullClientNameUseCase updateFullClientNameUseCase =
                new UpdateFullClientNameUseCase(app.getRepository());

        buttonSave.setOnClickListener(view -> {
            updateFullClientNameUseCase.execute(client,
                    nameEditText.getEditableText().toString(),
                    surnameEditText.getEditableText().toString(),
                    patronymicEditText.getEditableText().toString());
            Toast.makeText(this, R.string.data_saved, Toast.LENGTH_LONG).show();
            finish();
        });
    }
}