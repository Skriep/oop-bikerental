package by.skriep.bikerental.presentation.map;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.domain.usecases.auth.LoginByUsernameUseCase;
import by.skriep.bikerental.domain.usecases.map.GetBikeRentalPointsInRegionUseCase;

public class MapViewModelFactory implements ViewModelProvider.Factory {

    private final BikeRentalApp app;

    public MapViewModelFactory(BikeRentalApp app) {
        this.app = app;
    }

    @Override
    @NonNull
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(MapViewModel.class)) {
            return (T) new MapViewModel(
                    new GetBikeRentalPointsInRegionUseCase(app.getRepository()));
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}