package by.skriep.bikerental.presentation.client.profile.bike_trips;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.trips.BikeTrip;

public class BikeTripsAdapter extends RecyclerView.Adapter<BikeTripsAdapter.ViewHolder> {

    private List<BikeTrip> bikeTrips = new ArrayList<>();

    public void setBikeTrips(@NonNull List<BikeTrip> bikeTrips) {
        this.bikeTrips = bikeTrips;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tripStartDateTextView;
        private final TextView tripDurationTextView;

        public ViewHolder(View view) {
            super(view);
            tripStartDateTextView = view.findViewById(R.id.textViewTripStartDate);
            tripDurationTextView = view.findViewById(R.id.textViewTripDuration);
        }

        public TextView getTripStartDateTextView() {
            return tripStartDateTextView;
        }

        public TextView getTripDurationTextView() {
            return tripDurationTextView;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.bike_trip_row_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        BikeTrip bikeTrip = bikeTrips.get(position);
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy", new Locale("ru", "RU"));
        viewHolder.getTripStartDateTextView().setText(df.format(bikeTrip.getStartDate()));
        viewHolder.getTripDurationTextView().setText(String.format(new Locale("ru", "RU"),
                "%.1f %s", bikeTrip.getDuration() / 1000.0 / 60.0,
                viewHolder.getTripDurationTextView().getContext().getString(R.string.minute_short)));
    }

    @Override
    public int getItemCount() {
        return bikeTrips.size();
    }
}
