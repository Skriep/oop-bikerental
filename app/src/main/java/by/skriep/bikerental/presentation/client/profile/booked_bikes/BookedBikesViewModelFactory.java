package by.skriep.bikerental.presentation.client.profile.booked_bikes;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.data.repositories.BikeRentalRepository;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.usecases.bikes.CancelBicycleBookingUseCase;
import by.skriep.bikerental.domain.usecases.client.GetBookedByClientBikesUseCase;
import by.skriep.bikerental.domain.usecases.bikes.RentBicycleUseCase;

public class BookedBikesViewModelFactory implements ViewModelProvider.Factory {

    private final BikeRentalApp app;
    private final Client client;

    public BookedBikesViewModelFactory(BikeRentalApp app, Client client) {
        this.app = app;
        this.client = client;
    }

    @Override
    @NonNull
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(BookedBikesViewModel.class)) {
            BikeRentalRepository repository = app.getRepository();
            return (T) new BookedBikesViewModel(
                    new GetBookedByClientBikesUseCase(repository),
                    new CancelBicycleBookingUseCase(repository),
                    new RentBicycleUseCase(repository),
                    client);
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}