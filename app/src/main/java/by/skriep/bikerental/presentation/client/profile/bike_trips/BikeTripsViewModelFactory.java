package by.skriep.bikerental.presentation.client.profile.bike_trips;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.data.repositories.BikeRentalRepository;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.usecases.bikes.CancelBicycleBookingUseCase;
import by.skriep.bikerental.domain.usecases.bikes.RentBicycleUseCase;
import by.skriep.bikerental.domain.usecases.client.GetBookedByClientBikesUseCase;
import by.skriep.bikerental.domain.usecases.client.GetClientBikeTripsUseCase;

public class BikeTripsViewModelFactory implements ViewModelProvider.Factory {

    private final BikeRentalApp app;
    private final Client client;

    public BikeTripsViewModelFactory(BikeRentalApp app, Client client) {
        this.app = app;
        this.client = client;
    }

    @Override
    @NonNull
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(BikeTripsViewModel.class)) {
            BikeRentalRepository repository = app.getRepository();
            return (T) new BikeTripsViewModel(
                    new GetClientBikeTripsUseCase(repository),
                    client);
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}