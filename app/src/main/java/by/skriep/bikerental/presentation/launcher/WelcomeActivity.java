package by.skriep.bikerental.presentation.launcher;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.presentation.client.main.MainClientActivity;
import by.skriep.bikerental.presentation.login.LoginActivity;

public class WelcomeActivity extends AppCompatActivity {

    private BikeRentalApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (BikeRentalApp) getApplication();
        startLoginActivity();
    }

    ActivityResultLauncher<Intent> loginActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() != RESULT_OK) {
                    finish();
                } else {
                    startMainActivity();
                }
            });

    ActivityResultLauncher<Intent> mainActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                app.setLoggedInUser(null);
                startLoginActivity();
            });

    private void startLoginActivity() {
        loginActivityResultLauncher.launch(new Intent(this, LoginActivity.class));
    }

    private void startMainActivity() {
        mainActivityResultLauncher.launch(new Intent(this, MainClientActivity.class));
    }
}