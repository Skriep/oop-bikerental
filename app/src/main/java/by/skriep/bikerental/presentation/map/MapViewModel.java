package by.skriep.bikerental.presentation.map;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.yandex.mapkit.map.VisibleRegion;

import java.util.ArrayList;
import java.util.List;

import by.skriep.bikerental.data.relations.BikeRentalPointWithBicycles;
import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;
import by.skriep.bikerental.domain.usecases.map.GetBikeRentalPointsInRegionUseCase;

public class MapViewModel extends ViewModel {

    private final GetBikeRentalPointsInRegionUseCase getBikeRentalPointsInRegionUseCase;
    private final List<BikeRentalPoint> bikeRentalPoints = new ArrayList<>();
    private final MutableLiveData<RentalPointsLoadResult> rentalPointsLoadResultMutable = new MutableLiveData<>();

    public final LiveData<RentalPointsLoadResult> rentalPointsLoadResult = rentalPointsLoadResultMutable;
    public MapViewModel(@NonNull GetBikeRentalPointsInRegionUseCase getBikeRentalPointsInRegionUseCase) {
        this.getBikeRentalPointsInRegionUseCase = getBikeRentalPointsInRegionUseCase;
    }

    public void onMapVisibleRegionChanged(VisibleRegion visibleRegion) {
        List<BikeRentalPoint> newRentalPoints =
                getBikeRentalPointsInRegionUseCase.execute(visibleRegion, bikeRentalPoints);
        if (!newRentalPoints.isEmpty()) {
            bikeRentalPoints.addAll(newRentalPoints);
            rentalPointsLoadResultMutable.setValue(
                    new RentalPointsLoadResult(newRentalPoints, null));
        }
    }
}
