package by.skriep.bikerental.presentation.client.rental_point;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.data.repositories.BikeRentalRepository;
import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;
import by.skriep.bikerental.domain.usecases.bikes.BookBicycleUseCase;
import by.skriep.bikerental.domain.usecases.bikes.GetAvailableBikesAtRentalPointUseCase;
import by.skriep.bikerental.domain.usecases.bikes.RentBicycleUseCase;

public class BikeRentalPointViewModelFactory implements ViewModelProvider.Factory {

    private final BikeRentalApp app;
    private final BikeRentalPoint rentalPoint;

    public BikeRentalPointViewModelFactory(BikeRentalApp app, BikeRentalPoint rentalPoint) {
        this.app = app;
        this.rentalPoint = rentalPoint;
    }

    @Override
    @NonNull
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(BikeRentalPointViewModel.class)) {
            BikeRentalRepository repository = app.getRepository();
            return (T) new BikeRentalPointViewModel(
                    new GetAvailableBikesAtRentalPointUseCase(repository),
                    new BookBicycleUseCase(repository, repository),
                    new RentBicycleUseCase(repository),
                    rentalPoint);
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}