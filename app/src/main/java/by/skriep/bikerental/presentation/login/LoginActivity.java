package by.skriep.bikerental.presentation.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.users.User;
import by.skriep.bikerental.presentation.register.RegisterActivity;

public class LoginActivity extends AppCompatActivity {

    boolean usernameTextChanged = false;
    boolean passwordTextChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        BikeRentalApp app = (BikeRentalApp) getApplication();
        LoginViewModel viewModel =
                new ViewModelProvider(this, new LoginViewModelFactory(app))
                        .get(LoginViewModel.class);

        EditText usernameEditText = findViewById(R.id.editTextUsername);
        EditText passwordEditText = findViewById(R.id.editTextPassword);
        View loginButton = findViewById(R.id.buttonLogin);
        View goToRegistrationButton = findViewById(R.id.textViewRegister);

        viewModel.loginFormState.observe(this, loginFormState -> {
            if (loginFormState == null) {
                return;
            }
            loginButton.setEnabled(loginFormState.isDataValid());
            if (loginFormState.getUsernameError() != null && usernameTextChanged) {
                usernameEditText.setError(getString(loginFormState.getUsernameError()));
            }
            if (loginFormState.getPasswordError() != null && passwordTextChanged) {
                passwordEditText.setError(getString(loginFormState.getPasswordError()));
            }
        });

        viewModel.loginResult.observe(this, loginResult -> {
            if (loginResult == null) {
                return;
            }
            if (loginResult.getError() != null) {
                Toast.makeText(this, loginResult.getError(), Toast.LENGTH_LONG).show();
                return;
            }
            User loggedInUser = loginResult.getLoggedInUser();
            if (loggedInUser == null) {
                return;
            }
            app.setLoggedInUser(loggedInUser);
            setResult(RESULT_OK);
            finish();
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (!usernameTextChanged && s == usernameEditText.getEditableText()) {
                    usernameTextChanged = true;
                }
                if (!passwordTextChanged && s == passwordEditText.getEditableText()) {
                    passwordTextChanged = true;
                }
                viewModel.onLoginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);

        loginButton.setOnClickListener(view -> viewModel.login(
                usernameEditText.getText().toString(),
                passwordEditText.getText().toString()));

        goToRegistrationButton.setOnClickListener(view -> view.getContext()
                .startActivity(new Intent(this, RegisterActivity.class)));
    }
}