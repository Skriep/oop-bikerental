package by.skriep.bikerental.presentation.register;

import androidx.annotation.Nullable;

public class RegisterFormState {

    @Nullable
    private final Integer nameError;
    @Nullable
    private final Integer surnameError;
    @Nullable
    private final Integer patronymicError;
    @Nullable
    private final Integer usernameError;
    @Nullable
    private final Integer passwordError;
    @Nullable
    private final Integer passwordRepeatedError;

    RegisterFormState(@Nullable Integer nameError, @Nullable Integer surnameError,
                      @Nullable Integer patronymicError, @Nullable Integer usernameError,
                      @Nullable Integer passwordError, @Nullable Integer passwordRepeatedError) {
        this.nameError = nameError;
        this.surnameError = surnameError;
        this.patronymicError = patronymicError;
        this.usernameError = usernameError;
        this.passwordError = passwordError;
        this.passwordRepeatedError = passwordRepeatedError;
    }

    @Nullable
    Integer getNameError() {
        return nameError;
    }

    @Nullable
    Integer getSurnameError() {
        return surnameError;
    }

    @Nullable
    Integer getPatronymicError() {
        return patronymicError;
    }

    @Nullable
    Integer getUsernameError() {
        return usernameError;
    }

    @Nullable
    Integer getPasswordError() {
        return passwordError;
    }

    @Nullable
    Integer getPasswordRepeatedError() {
        return passwordRepeatedError;
    }

    boolean isDataValid() {
        return nameError == null && surnameError == null && patronymicError == null
                && usernameError == null && passwordError == null && passwordRepeatedError == null;
    }
}
