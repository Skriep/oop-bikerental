package by.skriep.bikerental.presentation.client.profile.booked_bikes;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.exceptions.bikes.BicycleBookedByAnotherClientException;
import by.skriep.bikerental.domain.exceptions.bikes.booking.BicycleRentedException;
import by.skriep.bikerental.domain.exceptions.bikes.renting.BicycleAlreadyRentedException;
import by.skriep.bikerental.domain.exceptions.bikes.renting.BicycleBookedException;
import by.skriep.bikerental.domain.exceptions.bikes.renting.RentingException;
import by.skriep.bikerental.domain.usecases.bikes.CancelBicycleBookingUseCase;
import by.skriep.bikerental.domain.usecases.bikes.RentBicycleUseCase;
import by.skriep.bikerental.domain.usecases.client.GetBookedByClientBikesUseCase;
import by.skriep.bikerental.presentation.client.rental_point.BicycleOperationResult;

public class BookedBikesViewModel extends ViewModel {

    private final CancelBicycleBookingUseCase cancelBicycleBookingUseCase;
    private final RentBicycleUseCase rentBicycleUseCase;
    private final MutableLiveData<BicycleOperationResult> bicycleBookingCancelResultMutable = new MutableLiveData<>();
    private final MutableLiveData<BicycleOperationResult> bicycleRentingResultMutable = new MutableLiveData<>();

    public final LiveData<List<Bicycle>> bicycles;
    public final LiveData<BicycleOperationResult> bicycleBookingCancelResult = bicycleBookingCancelResultMutable;
    public final LiveData<BicycleOperationResult> bicycleRentingResult = bicycleRentingResultMutable;

    public BookedBikesViewModel(@NonNull GetBookedByClientBikesUseCase getBookedByClientBikesUseCase,
                                @NonNull CancelBicycleBookingUseCase cancelBicycleBookingUseCase,
                                @NonNull RentBicycleUseCase rentBicycleUseCase,
                                @NonNull Client client) {
        this.cancelBicycleBookingUseCase = cancelBicycleBookingUseCase;
        this.rentBicycleUseCase = rentBicycleUseCase;
        this.bicycles = getBookedByClientBikesUseCase.execute(client);
    }

    public void rentBicycle(Client client, Bicycle bicycle) {
        try {
            rentBicycleUseCase.execute(client, bicycle);
            bicycleRentingResultMutable.setValue(new BicycleOperationResult.Success(bicycle, R.string.successfully_rented));
        } catch (BicycleBookedException e) {
            bicycleRentingResultMutable.setValue(new BicycleOperationResult.Error(bicycle, R.string.bicycle_booked_error));
        } catch (BicycleAlreadyRentedException e) {
            bicycleRentingResultMutable.setValue(new BicycleOperationResult.Error(bicycle, R.string.bicycle_rented_error));
        } catch (RentingException e) {
            bicycleRentingResultMutable.setValue(new BicycleOperationResult.Error(bicycle, R.string.unknown_error));
        }
    }

    public void cancelBookingBicycle(Client client, Bicycle bicycle) {
        try {
            cancelBicycleBookingUseCase.execute(client, bicycle);
            bicycleBookingCancelResultMutable.setValue(new BicycleOperationResult.Success(bicycle, R.string.successfully_cancelled_booking));
        } catch (BicycleRentedException e) {
            bicycleBookingCancelResultMutable.setValue(new BicycleOperationResult.Error(bicycle, R.string.bicycle_rented_error));
        } catch (BicycleBookedByAnotherClientException e) {
            bicycleBookingCancelResultMutable.setValue(new BicycleOperationResult.Error(bicycle, R.string.bicycle_booked_by_others_error));
        }
    }
}
