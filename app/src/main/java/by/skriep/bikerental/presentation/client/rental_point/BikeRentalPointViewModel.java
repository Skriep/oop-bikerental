package by.skriep.bikerental.presentation.client.rental_point;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.exceptions.bikes.booking.BicycleAlreadyBookedException;
import by.skriep.bikerental.domain.exceptions.bikes.booking.BicycleRentedException;
import by.skriep.bikerental.domain.exceptions.bikes.booking.BookingException;
import by.skriep.bikerental.domain.exceptions.bikes.booking.TooManyBookedException;
import by.skriep.bikerental.domain.exceptions.bikes.renting.BicycleAlreadyRentedException;
import by.skriep.bikerental.domain.exceptions.bikes.renting.BicycleBookedException;
import by.skriep.bikerental.domain.exceptions.bikes.renting.RentingException;
import by.skriep.bikerental.domain.usecases.bikes.BookBicycleUseCase;
import by.skriep.bikerental.domain.usecases.bikes.GetAvailableBikesAtRentalPointUseCase;
import by.skriep.bikerental.domain.usecases.bikes.RentBicycleUseCase;

public class BikeRentalPointViewModel extends ViewModel {

    private final BookBicycleUseCase bookBicycleUseCase;
    private final RentBicycleUseCase rentBicycleUseCase;
    private final MutableLiveData<BicycleOperationResult> bicycleBookingResultMutable = new MutableLiveData<>();
    private final MutableLiveData<BicycleOperationResult> bicycleRentingResultMutable = new MutableLiveData<>();

    public final LiveData<List<Bicycle>> bicycles;
    public final LiveData<BicycleOperationResult> bicycleBookingResult = bicycleBookingResultMutable;
    public final LiveData<BicycleOperationResult> bicycleRentingResult = bicycleRentingResultMutable;

    public BikeRentalPointViewModel(@NonNull GetAvailableBikesAtRentalPointUseCase getAvailableBikesAtRentalPointUseCase,
                                    @NonNull BookBicycleUseCase bookBicycleUseCase,
                                    @NonNull RentBicycleUseCase rentBicycleUseCase,
                                    @NonNull BikeRentalPoint rentalPoint) {
        this.bookBicycleUseCase = bookBicycleUseCase;
        this.rentBicycleUseCase = rentBicycleUseCase;
        this.bicycles = getAvailableBikesAtRentalPointUseCase.execute(rentalPoint);
    }

    public void bookBicycle(Client client, Bicycle bicycle) {
        try {
            bookBicycleUseCase.execute(client, bicycle);
            bicycleBookingResultMutable.setValue(new BicycleOperationResult.Success(bicycle, R.string.successfully_booked));
        } catch (BicycleAlreadyBookedException e) {
            bicycleBookingResultMutable.setValue(new BicycleOperationResult.Error(bicycle, R.string.bicycle_booked_error));
        } catch (BicycleRentedException e) {
            bicycleBookingResultMutable.setValue(new BicycleOperationResult.Error(bicycle, R.string.bicycle_rented_error));
        } catch (TooManyBookedException e) {
            bicycleBookingResultMutable.setValue(new BicycleOperationResult.Error(bicycle, R.string.max_booked_bicycles_error));
        } catch (BookingException e) {
            bicycleBookingResultMutable.setValue(new BicycleOperationResult.Error(bicycle, R.string.unknown_error));
        }
    }

    public void rentBicycle(Client client, Bicycle bicycle) {
        try {
            rentBicycleUseCase.execute(client, bicycle);
            bicycleRentingResultMutable.setValue(new BicycleOperationResult.Success(bicycle, R.string.successfully_rented));
        } catch (BicycleBookedException e) {
            bicycleRentingResultMutable.setValue(new BicycleOperationResult.Error(bicycle, R.string.bicycle_booked_error));
        } catch (BicycleAlreadyRentedException e) {
            bicycleRentingResultMutable.setValue(new BicycleOperationResult.Error(bicycle, R.string.bicycle_rented_error));
        } catch (RentingException e) {
            bicycleRentingResultMutable.setValue(new BicycleOperationResult.Error(bicycle, R.string.unknown_error));
        }
    }
}
