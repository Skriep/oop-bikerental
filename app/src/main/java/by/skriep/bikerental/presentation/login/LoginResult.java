package by.skriep.bikerental.presentation.login;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import by.skriep.bikerental.domain.entities.users.User;

public class LoginResult {
    @Nullable
    private User loggedInUser;
    @Nullable
    private Integer error;

    LoginResult(@NonNull Integer error) {
        this.error = error;
    }

    LoginResult(@NonNull User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    @Nullable
    User getLoggedInUser() {
        return loggedInUser;
    }

    @Nullable
    Integer getError() {
        return error;
    }
}
