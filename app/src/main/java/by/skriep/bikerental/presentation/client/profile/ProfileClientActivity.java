package by.skriep.bikerental.presentation.client.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.navigation.NavigationView;

import by.skriep.bikerental.R;
import by.skriep.bikerental.presentation.client.profile.bike_trips.BikeTripsClientActivity;
import by.skriep.bikerental.presentation.client.profile.booked_bikes.BookedBikesClientActivity;
import by.skriep.bikerental.presentation.client.profile.rented_bikes.RentedBikesClientActivity;

public class ProfileClientActivity extends AppCompatActivity {

    private static final int PRIVATE_INFO_GROUP_ID = 0;
    private static final int MENU_ITEM_PERSONAL_INFO_ID = 0;
    private static final int MENU_ITEM_CHANGE_PASSWORD_ID = 1;

    private static final int BIKES_GROUP_ID = 1;
    private static final int MENU_ITEM_BOOKED_BIKES_ID = 10;
    private static final int MENU_ITEM_RENTED_BIKES_ID = 11;
    private static final int MENU_ITEM_FINISHED_TRIPS_ID = 12;

    private static final int LOGOUT_GROUP_ID = 2;
    private static final int MENU_ITEM_LOGOUT_ID = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_client);

        NavigationView navigationView = findViewById(R.id.navigationView);
        Menu menu = navigationView.getMenu();
        menu.add(PRIVATE_INFO_GROUP_ID, MENU_ITEM_PERSONAL_INFO_ID, Menu.NONE, R.string.personal_info_menu_item)
                .setIcon(R.drawable.ic_person_24);
        menu.add(PRIVATE_INFO_GROUP_ID, MENU_ITEM_CHANGE_PASSWORD_ID, Menu.NONE, R.string.change_password_menu_item)
                .setIcon(R.drawable.ic_lock_24);
        menu.add(BIKES_GROUP_ID, MENU_ITEM_BOOKED_BIKES_ID, Menu.NONE, R.string.booked_bikes_menu_item)
                .setIcon(R.drawable.ic_pedal_bike_36);
        menu.add(BIKES_GROUP_ID, MENU_ITEM_RENTED_BIKES_ID, Menu.NONE, R.string.rented_bikes_menu_item)
                .setIcon(R.drawable.ic_directions_bike_24);
        menu.add(BIKES_GROUP_ID, MENU_ITEM_FINISHED_TRIPS_ID, Menu.NONE, R.string.finished_trips_menu_item)
                .setIcon(R.drawable.ic_timeline_24);
        menu.add(LOGOUT_GROUP_ID, MENU_ITEM_LOGOUT_ID, Menu.NONE, R.string.logout_menu_item)
                .setIcon(R.drawable.ic_keyboard_return_24);

        navigationView.setNavigationItemSelectedListener(item -> {
            switch (item.getGroupId()) {
                case PRIVATE_INFO_GROUP_ID:
                    switch (item.getItemId()) {
                        case MENU_ITEM_PERSONAL_INFO_ID:
                            startActivity(new Intent(this, PersonalInfoClientActivity.class));
                            return true;
                        case MENU_ITEM_CHANGE_PASSWORD_ID:
                            startActivity(new Intent(this, PasswordChangeClientActivity.class));
                            return true;
                    }
                    break;

                case BIKES_GROUP_ID:
                    switch (item.getItemId()) {
                        case MENU_ITEM_BOOKED_BIKES_ID:
                            startActivity(new Intent(this, BookedBikesClientActivity.class));
                            return true;
                        case MENU_ITEM_RENTED_BIKES_ID:
                            startActivity(new Intent(this, RentedBikesClientActivity.class));
                            return true;
                        case MENU_ITEM_FINISHED_TRIPS_ID:
                            startActivity(new Intent(this, BikeTripsClientActivity.class));
                            return true;
                    }
                    break;

                case LOGOUT_GROUP_ID:
                    if (item.getItemId() == MENU_ITEM_LOGOUT_ID) {
                        setResult(RESULT_FIRST_USER);
                        finish();
                        return true;
                    }
                    break;
            }
            return false;
        });
    }
}