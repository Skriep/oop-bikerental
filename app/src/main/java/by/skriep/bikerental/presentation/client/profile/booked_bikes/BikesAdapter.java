package by.skriep.bikerental.presentation.client.profile.booked_bikes;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.bikes.Bicycle;

interface BikeClickListener {

    void onCancelBooking(Bicycle bicycle);

    void onRent(Bicycle bicycle);
}

public class BikesAdapter extends RecyclerView.Adapter<BikesAdapter.ViewHolder>
        implements View.OnClickListener {

    private final BikeClickListener bikeClickListener;

    private List<Bicycle> bicycles = new ArrayList<>();

    @Override
    public void onClick(View v) {
        Object tag = v.getTag();
        if (!(tag instanceof Bicycle)) {
            return;
        }
        Bicycle bicycle = (Bicycle) tag;
        if (v.getId() == R.id.imageViewMenu) {
            showPopupMenu(v, bicycle);
        }
    }

    public BikesAdapter(@NonNull BikeClickListener bikeClickListener) {
        this.bikeClickListener = bikeClickListener;
    }

    public void setBicycles(@NonNull List<Bicycle> bicycles) {
        this.bicycles = bicycles;
        notifyDataSetChanged();
    }

    private static final int ID_CANCEL_BOOKING = 0;
    private static final int ID_RENT = 1;

    private void showPopupMenu(View view, Bicycle bicycle) {
        PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
        popupMenu.getMenu().add(0, ID_CANCEL_BOOKING, Menu.NONE, R.string.cancel_booking_option);
        popupMenu.getMenu().add(0, ID_RENT, Menu.NONE, R.string.rent_bike_option);

        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case ID_CANCEL_BOOKING:
                    bikeClickListener.onCancelBooking(bicycle);
                    break;
                case ID_RENT:
                    bikeClickListener.onRent(bicycle);
                    break;
                default:
                    return false;
            }
            return true;
        });

        popupMenu.show();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView bicycleIdCodeTextView;
        private final TextView pricePerMinuteTextView;
        private final View menuButton;

        public ViewHolder(View view) {
            super(view);
            bicycleIdCodeTextView = view.findViewById(R.id.textViewBicycleIdCode);
            pricePerMinuteTextView = view.findViewById(R.id.textViewPricePerMinute);
            menuButton = view.findViewById(R.id.imageViewMenu);
        }

        public TextView getBicycleIdCodeTextView() {
            return bicycleIdCodeTextView;
        }

        public TextView getPricePerMinuteTextView() {
            return pricePerMinuteTextView;
        }

        public View getMenuButton() {
            return menuButton;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.bicycle_row_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.menuButton.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Bicycle bicycle = bicycles.get(position);
        viewHolder.getBicycleIdCodeTextView().setText(bicycle.getIdCode());
        viewHolder.getPricePerMinuteTextView().setText(String.format(new Locale("ru", "RU"),
                "%.2f / %s", bicycle.getCostPerMinute(),
                viewHolder.getPricePerMinuteTextView().getContext().getString(R.string.minute_short)));
        viewHolder.getMenuButton().setTag(bicycle);
    }

    @Override
    public int getItemCount() {
        return bicycles.size();
    }
}
