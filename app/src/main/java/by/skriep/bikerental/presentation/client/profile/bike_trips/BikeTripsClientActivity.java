package by.skriep.bikerental.presentation.client.profile.bike_trips;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import by.skriep.bikerental.BikeRentalApp;
import by.skriep.bikerental.R;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.entities.users.User;

public class BikeTripsClientActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bike_trips_client);

        BikeRentalApp app = (BikeRentalApp) getApplication();

        User loggedInUser = app.getLoggedInUser();
        if (!(loggedInUser instanceof Client)) {
            Toast.makeText(this, R.string.not_a_client_error, Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        Client client = (Client) loggedInUser;

        RecyclerView recyclerView = findViewById(R.id.recyclerViewBikeTrips);
        View emptyView = findViewById(R.id.textViewEmpty);

        BikeTripsAdapter adapter = new BikeTripsAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        BikeTripsViewModel viewModel = new ViewModelProvider(this, new BikeTripsViewModelFactory(app, client))
                .get(BikeTripsViewModel.class);

        viewModel.bikeTrips.observe(this, bikeTrips -> {
            adapter.setBikeTrips(bikeTrips);
            if (bikeTrips.isEmpty()) {
                emptyView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            } else {
                emptyView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        });
    }
}