package by.skriep.bikerental;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.room.Room;

import com.yandex.mapkit.MapKitFactory;

import by.skriep.bikerental.data.BikeRentalDatabase;
import by.skriep.bikerental.data.repositories.BikeRentalRepository;
import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;
import by.skriep.bikerental.domain.entities.users.User;

public class BikeRentalApp extends Application {

    private User loggedInUser = null;
    private BikeRentalPoint selectedRentalPoint = null;
    private BikeRentalDatabase database;
    private BikeRentalRepository repository;

    @Override
    public void onCreate() {
        super.onCreate();

        database = Room.databaseBuilder(getApplicationContext(),
                BikeRentalDatabase.class, "bikerental-db")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();

        repository = new BikeRentalRepository(database);

        MapKitFactory.setApiKey(BuildConfig.MapkitApiKey);
    }

    public User getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(@Nullable User user) {
        loggedInUser = user;
    }

    public BikeRentalRepository getRepository() {
        return repository;
    }

    public BikeRentalPoint getSelectedRentalPoint() {
        return selectedRentalPoint;
    }

    public void setSelectedRentalPoint(BikeRentalPoint rentalPoint) {
        this.selectedRentalPoint = rentalPoint;
    }
}
