package by.skriep.bikerental.domain.usecases.bikes;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import java.util.List;

import by.skriep.bikerental.data.repositories.BicyclesRepository;
import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;

public class GetAvailableBikesAtRentalPointUseCase {

    private final BicyclesRepository repository;

    public GetAvailableBikesAtRentalPointUseCase(@NonNull BicyclesRepository repository) {
        this.repository = repository;
    }

    public LiveData<List<Bicycle>> execute(BikeRentalPoint rentalPoint) {
        return repository.getAvailableBicyclesLiveData(rentalPoint);
    }
}
