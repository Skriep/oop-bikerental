package by.skriep.bikerental.domain.usecases.bikes;

import androidx.annotation.NonNull;

import java.util.Date;

import by.skriep.bikerental.data.repositories.BicyclesRepository;
import by.skriep.bikerental.data.repositories.BikeTripsRepository;
import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.trips.BikeTrip;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.exceptions.bikes.BicycleNotRentedException;
import by.skriep.bikerental.domain.exceptions.bikes.BicycleRentedByAnotherClientException;

public class FinishBicycleRentingUseCase {

    private final BicyclesRepository bicyclesRepository;
    private final BikeTripsRepository bikeTripsRepository;

    public FinishBicycleRentingUseCase(@NonNull BicyclesRepository bicyclesRepository,
                                       @NonNull BikeTripsRepository bikeTripsRepository) {
        this.bicyclesRepository = bicyclesRepository;
        this.bikeTripsRepository = bikeTripsRepository;
    }

    public BikeTrip execute(Client client, Bicycle bicycle)
            throws BicycleNotRentedException, BicycleRentedByAnotherClientException {
        if (bicycle.getRentedByClientId() == null) {
            throw new BicycleNotRentedException();
        }
        if (bicycle.getRentedByClientId() != client.getId()) {
            throw new BicycleRentedByAnotherClientException();
        }
        Date rentalStarted = bicycle.getRentalStartTime();
        if (rentalStarted == null) {
            throw new NullPointerException();
        }
        long rentalPeriod = new Date().getTime() - rentalStarted.getTime();
        bicycle.setRentedByClientId(null);
        bicycle.setBookedByClientId(null);
        bicycle.setRentalStartTime(null);
        bicyclesRepository.updateBicycles(bicycle);
        BikeTrip bikeTrip = new BikeTrip(rentalPeriod, client, rentalStarted);
        bikeTripsRepository.addNewBikeTrip(bikeTrip);
        return bikeTrip;
    }
}
