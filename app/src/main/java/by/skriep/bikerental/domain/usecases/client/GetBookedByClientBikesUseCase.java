package by.skriep.bikerental.domain.usecases.client;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import java.util.List;

import by.skriep.bikerental.data.repositories.BicyclesRepository;
import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.users.Client;

public class GetBookedByClientBikesUseCase {

    private final BicyclesRepository repository;

    public GetBookedByClientBikesUseCase(@NonNull BicyclesRepository repository) {
        this.repository = repository;
    }

    public LiveData<List<Bicycle>> execute(Client client) {
        return repository.getBookedByClientBicyclesLiveData(client);
    }
}
