package by.skriep.bikerental.domain.entities.users;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;

@Entity(inheritSuperIndices = true)
public class Client extends User {

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "surname")
    private String surname;

    @ColumnInfo(name = "patronymic")
    private String patronymic;

    @Ignore
    public Client(String username, String passwordHash, String passwordSalt,
                  String name, String surname, String patronymic) {
        this(0, username, passwordHash, passwordSalt, name, surname, patronymic);
    }

    public Client(long id, String username, String passwordHash, String passwordSalt,
                  String name, String surname, String patronymic) {
        super(id, username, passwordHash, passwordSalt);
        setName(name);
        setSurname(surname);
        setPatronymic(patronymic);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }
}
