package by.skriep.bikerental.domain.entities.users;

import androidx.room.ColumnInfo;
import androidx.room.Index;

import by.skriep.bikerental.domain.entities.Entity;

@androidx.room.Entity(indices = {@Index(value = {"username"}, unique = true)})
public abstract class User extends Entity {

    @ColumnInfo(name = "username")
    private String username = "";

    @ColumnInfo(name = "password_hash")
    private String passwordHash = "";

    @ColumnInfo(name = "password_salt")
    private String passwordSalt = "";

    public User(long id, String username, String passwordHash, String passwordSalt) {
        super(id);
        setUsername(username);
        setPasswordHash(passwordHash);
        setPasswordSalt(passwordSalt);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getPasswordSalt() {
        return passwordSalt;
    }

    public void setPasswordSalt(String passwordSalt) {
        this.passwordSalt = passwordSalt;
    }
}
