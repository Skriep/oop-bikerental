package by.skriep.bikerental.domain.entities;

import androidx.room.PrimaryKey;

@androidx.room.Entity
public abstract class Entity {

    @PrimaryKey(autoGenerate = true)
    private long id;

    public Entity(long id) {
        setId(id);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
