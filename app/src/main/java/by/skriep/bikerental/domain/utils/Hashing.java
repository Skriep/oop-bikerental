package by.skriep.bikerental.domain.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hashing {

    public static String sha512(byte[] data) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
            byte[] hash = messageDigest.digest(data);
            return Conversion.bytesToHex(hash);
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    public static String sha512(String data) {
        return sha512(Conversion.stringToBytes(data));
    }
}
