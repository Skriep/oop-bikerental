package by.skriep.bikerental.domain.utils;

public class MathUtils {

    public static double min(double first, double... values) {
        double result = first;
        for (double value: values) {
            if (value < result) {
                result = value;
            }
        }
        return result;
    }

    public static double max(double first, double... values) {
        double result = first;
        for (double value: values) {
            if (value > result) {
                result = value;
            }
        }
        return result;
    }
}
