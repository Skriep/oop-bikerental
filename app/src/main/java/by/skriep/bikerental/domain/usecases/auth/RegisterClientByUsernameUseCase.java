package by.skriep.bikerental.domain.usecases.auth;

import androidx.annotation.NonNull;

import by.skriep.bikerental.data.repositories.UsersRepository;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.entities.users.User;
import by.skriep.bikerental.domain.exceptions.auth.UsernameAlreadyRegisteredException;
import by.skriep.bikerental.domain.utils.PasswordManagement;

public class RegisterClientByUsernameUseCase {

    private final UsersRepository repository;

    public RegisterClientByUsernameUseCase(@NonNull UsersRepository repository) {
        this.repository = repository;
    }

    public Client execute(String name, String surname, String patronymic,
                        String username, String password)
            throws UsernameAlreadyRegisteredException {
        User user = repository.findUserByUsername(username);
        if (user != null) {
            throw new UsernameAlreadyRegisteredException();
        }
        PasswordManagement.PasswordHashingResult hashingResult =
                PasswordManagement.hashPassword(password);
        Client client = new Client( username, hashingResult.getPasswordHash(),
                hashingResult.getPasswordSalt(), name, surname, patronymic);
        repository.addNewClient(client);
        return client;
    }
}
