package by.skriep.bikerental.domain.utils;

import androidx.annotation.NonNull;

public class PasswordManagement {

    public static PasswordHashingResult hashPassword(String password) {
        return hashPassword(password, Random.getRandomHexString(30));
    }

    public static PasswordHashingResult hashPassword(String password, String salt) {
        return new PasswordHashingResult(Hashing.sha512(password + salt), salt);
    }

    public static boolean isPasswordValid(String password, String passwordHash, String passwordSalt) {
        PasswordHashingResult hashingResult = hashPassword(password, passwordSalt);
        return passwordHash.equalsIgnoreCase(hashingResult.getPasswordHash());
    }

    public static class PasswordHashingResult {
        private final String passwordHash;
        private final String passwordSalt;

        public PasswordHashingResult(@NonNull String passwordHash, @NonNull String passwordSalt) {
            this.passwordHash = passwordHash;
            this.passwordSalt = passwordSalt;
        }

        public String getPasswordHash() {
            return passwordHash;
        }

        public String getPasswordSalt() {
            return passwordSalt;
        }
    }
}
