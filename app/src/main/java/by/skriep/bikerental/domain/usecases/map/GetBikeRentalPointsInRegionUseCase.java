package by.skriep.bikerental.domain.usecases.map;

import androidx.annotation.NonNull;

import com.yandex.mapkit.map.VisibleRegion;

import java.util.List;

import by.skriep.bikerental.data.repositories.BikeRentalPointsRepository;
import by.skriep.bikerental.domain.entities.Entity;
import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;
import by.skriep.bikerental.domain.utils.MathUtils;

public class GetBikeRentalPointsInRegionUseCase {

    private final BikeRentalPointsRepository repository;

    public GetBikeRentalPointsInRegionUseCase(@NonNull BikeRentalPointsRepository repository) {
        this.repository = repository;
    }

    public List<BikeRentalPoint> execute(VisibleRegion region, List<BikeRentalPoint> ignored) {
        double minLatitude = MathUtils.min(
                region.getBottomLeft().getLatitude(),
                region.getBottomRight().getLatitude(),
                region.getTopLeft().getLatitude(),
                region.getTopRight().getLatitude());
        double maxLatitude = MathUtils.max(
                region.getBottomLeft().getLatitude(),
                region.getBottomRight().getLatitude(),
                region.getTopLeft().getLatitude(),
                region.getTopRight().getLatitude());
        double minLongitude = MathUtils.min(
                region.getBottomLeft().getLongitude(),
                region.getBottomRight().getLongitude(),
                region.getTopLeft().getLongitude(),
                region.getTopRight().getLongitude());
        double maxLongitude = MathUtils.max(
                region.getBottomLeft().getLongitude(),
                region.getBottomRight().getLongitude(),
                region.getTopLeft().getLongitude(),
                region.getTopRight().getLongitude());

        long[] ignoredIds = ignored.stream().mapToLong(Entity::getId).toArray();
        return repository.getBikeRentalPointsInRegion(
                minLatitude, minLongitude, maxLatitude, maxLongitude, ignoredIds);
    }
}
