package by.skriep.bikerental.domain.utils;

import androidx.annotation.Nullable;
import androidx.room.TypeConverter;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Date;

public class Conversion {

    public static byte[] stringToBytes(String string) {
        return string.getBytes(StandardCharsets.UTF_8);
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuilder stringBuilder = new StringBuilder(bytes.length * 2);
        for (byte b : bytes) {
            stringBuilder.append(String.format("%02x", b));
        }
        return stringBuilder.toString();
    }

    @TypeConverter
    public static String bigDecimalToString(@Nullable BigDecimal input) {
        return input != null ? input.toPlainString() : "";
    }

    @TypeConverter
    public static BigDecimal stringToBigDecimal(@Nullable String input) {
        if (input == null || input.isEmpty()) {
            return BigDecimal.ZERO;
        }
        return new BigDecimal(input);
    }

    @TypeConverter
    public static long dateToLong(@Nullable Date input) {
        return input != null ? input.getTime() : 0;
    }

    @TypeConverter
    public static Date longToDate(@Nullable Long input) {
        if (input == null) {
            return new Date(0);
        }
        return new Date(input);
    }
}
