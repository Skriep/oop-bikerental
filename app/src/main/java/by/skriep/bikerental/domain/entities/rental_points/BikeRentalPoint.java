package by.skriep.bikerental.domain.entities.rental_points;

import androidx.room.ColumnInfo;

import com.yandex.mapkit.geometry.Point;

import by.skriep.bikerental.domain.entities.Entity;

@androidx.room.Entity
public class BikeRentalPoint extends Entity {

    @ColumnInfo(name = "latitude")
    private double latitude;

    @ColumnInfo(name = "longitude")
    private double longitude;

    public BikeRentalPoint(long id, Point position) {
        this(id, position.getLatitude(), position.getLongitude());
    }

    public BikeRentalPoint(long id, double latitude, double longitude) {
        super(id);
        setLatitude(latitude);
        setLongitude(longitude);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
