package by.skriep.bikerental.domain.usecases.bikes;

import androidx.annotation.NonNull;

import by.skriep.bikerental.data.repositories.BicyclesRepository;
import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.exceptions.bikes.BicycleBookedByAnotherClientException;
import by.skriep.bikerental.domain.exceptions.bikes.booking.BicycleRentedException;

public class CancelBicycleBookingUseCase {

    private final BicyclesRepository repository;

    public CancelBicycleBookingUseCase(@NonNull BicyclesRepository repository) {
        this.repository = repository;
    }

    public void execute(Client client, Bicycle bicycle)
            throws BicycleRentedException, BicycleBookedByAnotherClientException {
        if (bicycle.getRentedByClientId() != null) {
            throw new BicycleRentedException();
        }
        if (bicycle.getBookedByClientId() == null) {
            return;
        }
        if (bicycle.getBookedByClientId() != client.getId()) {
            throw new BicycleBookedByAnotherClientException();
        }
        bicycle.setBookedByClientId(null);
        bicycle.setBookingStartTime(null);
        repository.updateBicycles(bicycle);
    }
}
