package by.skriep.bikerental.domain.usecases.client;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import java.util.List;

import by.skriep.bikerental.data.repositories.BikeTripsRepository;
import by.skriep.bikerental.domain.entities.trips.BikeTrip;
import by.skriep.bikerental.domain.entities.users.Client;

public class GetClientBikeTripsUseCase {

    private final BikeTripsRepository repository;

    public GetClientBikeTripsUseCase(@NonNull BikeTripsRepository repository) {
        this.repository = repository;
    }

    public LiveData<List<BikeTrip>> execute(Client client) {
        return repository.getAllBikeTripsLiveData(client);
    }
}
