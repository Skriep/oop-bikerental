package by.skriep.bikerental.domain.entities.bikes;

import androidx.room.Entity;

@Entity
public class BikeLock {

    private String lockCode;

    public BikeLock(String lockCode) {
        this.lockCode = lockCode;
    }

    public String getLockCode() {
        return lockCode;
    }

    public void setLockCode(String lockCode) {
        this.lockCode = lockCode;
    }
}
