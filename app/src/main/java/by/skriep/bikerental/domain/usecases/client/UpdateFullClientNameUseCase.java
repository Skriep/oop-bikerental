package by.skriep.bikerental.domain.usecases.client;

import androidx.annotation.NonNull;

import by.skriep.bikerental.data.repositories.ClientsRepository;
import by.skriep.bikerental.domain.entities.users.Client;

public class UpdateFullClientNameUseCase {

    private final ClientsRepository clientsRepository;

    public UpdateFullClientNameUseCase(@NonNull ClientsRepository clientsRepository) {
        this.clientsRepository = clientsRepository;
    }

    public void execute(Client client, String name, String surname, String patronymic) {
        client.setName(name);
        client.setSurname(surname);
        client.setPatronymic(patronymic);
        clientsRepository.updateClients(client);
    }
}
