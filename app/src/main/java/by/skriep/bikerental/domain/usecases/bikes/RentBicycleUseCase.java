package by.skriep.bikerental.domain.usecases.bikes;

import androidx.annotation.NonNull;

import java.util.Date;

import by.skriep.bikerental.data.repositories.BicyclesRepository;
import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.exceptions.bikes.renting.BicycleAlreadyRentedException;
import by.skriep.bikerental.domain.exceptions.bikes.renting.BicycleBookedException;
import by.skriep.bikerental.domain.exceptions.bikes.renting.RentingException;

public class RentBicycleUseCase {

    private final BicyclesRepository bicyclesRepository;

    public RentBicycleUseCase(@NonNull BicyclesRepository bicyclesRepository) {
        this.bicyclesRepository = bicyclesRepository;
    }

    public void execute(Client client, Bicycle bicycle) throws RentingException {

        if (bicycle.getRentedByClientId() != null) {
            throw new BicycleAlreadyRentedException();
        }
        if (bicycle.getBookedByClientId() != null
                && bicycle.getBookedByClientId() != client.getId()) {
            throw new BicycleBookedException();
        }
        bicycle.setBookedByClientId(null);
        bicycle.setRentedByClientId(client.getId());
        bicycle.setRentalStartTime(new Date());
        bicyclesRepository.updateBicycles(bicycle);
    }
}
