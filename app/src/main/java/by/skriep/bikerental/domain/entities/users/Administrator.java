package by.skriep.bikerental.domain.entities.users;

import androidx.room.Entity;
import androidx.room.Ignore;

@Entity(inheritSuperIndices = true)
public class Administrator extends User {

    @Ignore
    public Administrator(String username, String passwordHash, String passwordSalt) {
        this(0, username, passwordHash, passwordSalt);
    }

    public Administrator(long id, String username, String passwordHash, String passwordSalt) {
        super(id, username, passwordHash, passwordSalt);
    }
}
