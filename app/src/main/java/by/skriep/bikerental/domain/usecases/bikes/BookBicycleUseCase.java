package by.skriep.bikerental.domain.usecases.bikes;

import androidx.annotation.NonNull;

import java.util.Date;

import by.skriep.bikerental.data.relations.ClientWithBookedBicycles;
import by.skriep.bikerental.data.repositories.BicyclesRepository;
import by.skriep.bikerental.data.repositories.ClientsRepository;
import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.exceptions.bikes.booking.BicycleAlreadyBookedException;
import by.skriep.bikerental.domain.exceptions.bikes.booking.BicycleRentedException;
import by.skriep.bikerental.domain.exceptions.bikes.booking.BookingException;
import by.skriep.bikerental.domain.exceptions.bikes.booking.TooManyBookedException;

public class BookBicycleUseCase {

    private final BicyclesRepository bicyclesRepository;
    private final ClientsRepository clientsRepository;

    public BookBicycleUseCase(@NonNull BicyclesRepository bicyclesRepository,
                              @NonNull ClientsRepository clientsRepository) {
        this.bicyclesRepository = bicyclesRepository;
        this.clientsRepository = clientsRepository;
    }

    public void execute(Client client, Bicycle bicycle) throws BookingException {

        if (bicycle.getRentedByClientId() != null) {
            throw new BicycleRentedException();
        }
        if (bicycle.getBookedByClientId() != null) {
            throw new BicycleAlreadyBookedException();
        }
        ClientWithBookedBicycles clientWithBookedBicycles =
                clientsRepository.getClientWithBookedBicyclesById(client.getId());
        if (clientWithBookedBicycles.bookedBicycles.size() >= 5) {
            throw new TooManyBookedException();
        }
        bicycle.setBookedByClientId(client.getId());
        bicycle.setBookingStartTime(new Date());
        bicyclesRepository.updateBicycles(bicycle);
    }
}
