package by.skriep.bikerental.domain.usecases.client;

import androidx.annotation.NonNull;

import by.skriep.bikerental.data.repositories.UsersRepository;
import by.skriep.bikerental.domain.entities.users.User;
import by.skriep.bikerental.domain.exceptions.auth.InvalidPasswordException;
import by.skriep.bikerental.domain.utils.PasswordManagement;

public class ChangeUserPasswordUseCase {

    private final UsersRepository repository;

    public ChangeUserPasswordUseCase(@NonNull UsersRepository repository) {
        this.repository = repository;
    }

    public void execute(User user, String oldPassword, String newPassword)
            throws InvalidPasswordException {
        if (!PasswordManagement.isPasswordValid(oldPassword, user.getPasswordHash(),
                user.getPasswordSalt())) {
            throw new InvalidPasswordException();
        }
        PasswordManagement.PasswordHashingResult hashingResult =
                PasswordManagement.hashPassword(newPassword);
        user.setPasswordHash(hashingResult.getPasswordHash());
        user.setPasswordSalt(hashingResult.getPasswordSalt());
        repository.updateUser(user);
    }
}
