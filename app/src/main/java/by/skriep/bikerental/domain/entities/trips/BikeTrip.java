package by.skriep.bikerental.domain.entities.trips;

import androidx.room.ColumnInfo;
import androidx.room.TypeConverters;

import java.util.Date;

import by.skriep.bikerental.domain.entities.Entity;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.utils.Conversion;

@androidx.room.Entity
public class BikeTrip extends Entity {

    @ColumnInfo(name = "duration")
    private long duration;

    @ColumnInfo(name = "client_id")
    private long clientId;

    @ColumnInfo(name = "start_date")
    @TypeConverters(Conversion.class)
    private Date startDate;

    public BikeTrip(long duration, Client client, Date startDate) {
        this(0, duration, client.getId(), startDate);
    }

    public BikeTrip(long id, long duration, long clientId, Date startDate) {
        super(id);
        this.duration = duration;
        this.clientId = clientId;
        this.startDate = startDate;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
