package by.skriep.bikerental.domain.entities.bikes;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.TypeConverters;

import java.math.BigDecimal;
import java.util.Date;

import by.skriep.bikerental.domain.entities.Entity;
import by.skriep.bikerental.domain.utils.Conversion;

@androidx.room.Entity
public class Bicycle extends Entity {

    @Nullable
    @ColumnInfo(name = "rental_point_id")
    private Long rentalPointId;

    @Nullable
    @ColumnInfo(name = "rented_by_client_id")
    private Long rentedByClientId;

    @Nullable
    @ColumnInfo(name = "rental_start_time")
    @TypeConverters(Conversion.class)
    private Date rentalStartTime;

    @Nullable
    @ColumnInfo(name = "booked_by_client_id")
    private Long bookedByClientId;

    @Nullable
    @ColumnInfo(name = "booking_start_time")
    @TypeConverters(Conversion.class)
    private Date bookingStartTime;

    @ColumnInfo(name = "cost_per_minute")
    @TypeConverters(Conversion.class)
    private BigDecimal costPerMinute;

    @ColumnInfo(name = "id_code")
    private String idCode;

    @Embedded
    private BikeLock bikeLock;

    public Bicycle(long id, BigDecimal costPerMinute,
                   String idCode,
                   BikeLock bikeLock,
                   @Nullable Long rentalPointId,
                   @Nullable Long rentedByClientId,
                   @Nullable Date rentalStartTime,
                   @Nullable Long bookedByClientId,
                   @Nullable Date bookingStartTime) {
        super(id);
        setRentalStartTime(rentalStartTime);
        setBookingStartTime(bookingStartTime);
        setCostPerMinute(costPerMinute);
        setIdCode(idCode);
        setBikeLock(bikeLock);
        setRentalPointId(rentalPointId);
        setRentedByClientId(rentedByClientId);
        setBookedByClientId(bookedByClientId);
    }

    @Nullable
    public Long getRentalPointId() {
        return rentalPointId;
    }

    public void setRentalPointId(@Nullable Long rentalPointId) {
        this.rentalPointId = rentalPointId;
    }

    public BigDecimal getCostPerMinute() {
        return costPerMinute;
    }

    public void setCostPerMinute(BigDecimal costPerMinute) {
        this.costPerMinute = costPerMinute;
    }

    @Nullable
    public Long getRentedByClientId() {
        return rentedByClientId;
    }

    public void setRentedByClientId(@Nullable Long rentedByClientId) {
        this.rentedByClientId = rentedByClientId;
    }

    public BikeLock getBikeLock() {
        return bikeLock;
    }

    public void setBikeLock(BikeLock bikeLock) {
        this.bikeLock = bikeLock;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    @Nullable
    public Long getBookedByClientId() {
        return bookedByClientId;
    }

    public void setBookedByClientId(@Nullable Long bookedByClientId) {
        this.bookedByClientId = bookedByClientId;
    }

    @Nullable
    public Date getRentalStartTime() {
        return rentalStartTime;
    }

    public void setRentalStartTime(@Nullable Date rentalStartTime) {
        this.rentalStartTime = rentalStartTime;
    }

    @Nullable
    public Date getBookingStartTime() {
        return bookingStartTime;
    }

    public void setBookingStartTime(@Nullable Date bookingStartTime) {
        this.bookingStartTime = bookingStartTime;
    }
}
