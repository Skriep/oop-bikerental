package by.skriep.bikerental.domain.utils;

import java.security.SecureRandom;

public class Random {

    private static final SecureRandom secureRandom = new SecureRandom();

    public static byte[] getRandomBytes(int length) {
        if (length <= 0) {
            return new byte[0];
        }
        secureRandom.setSeed(secureRandom.generateSeed(30));
        byte[] bytes = new byte[length];
        secureRandom.nextBytes(bytes);
        return bytes;
    }

    public static String getRandomHexString(int length) {
        if (length <= 0) {
            return "";
        }
        int bytesLength = length / 2;
        if (length % 2 != 0) {
            bytesLength++;
        }
        String hexString = Conversion.bytesToHex(getRandomBytes(bytesLength));
        if (hexString.length() > length) {
            return hexString.substring(0, hexString.length() - 1);
        }
        return hexString;
    }
}
