package by.skriep.bikerental.domain.usecases.auth;

import androidx.annotation.NonNull;

import by.skriep.bikerental.data.repositories.UsersRepository;
import by.skriep.bikerental.domain.entities.users.User;
import by.skriep.bikerental.domain.exceptions.auth.InvalidPasswordException;
import by.skriep.bikerental.domain.exceptions.auth.NoSuchUserException;
import by.skriep.bikerental.domain.utils.PasswordManagement;

public class LoginByUsernameUseCase {

    private final UsersRepository repository;

    public LoginByUsernameUseCase(@NonNull UsersRepository repository) {
        this.repository = repository;
    }

    public User execute(String username, String password)
            throws NoSuchUserException, InvalidPasswordException {
        User user = repository.findUserByUsername(username);
        if (user == null) {
            throw new NoSuchUserException();
        }
        if (!PasswordManagement.isPasswordValid(password, user.getPasswordHash(),
                user.getPasswordSalt())) {
            throw new InvalidPasswordException();
        }
        return user;
    }
}
