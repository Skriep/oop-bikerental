package by.skriep.bikerental.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;

@Dao
public interface BicycleDao {
    @Query("SELECT * FROM bicycle")
    List<Bicycle> getAll();

    @Query("SELECT * FROM bicycle WHERE rental_point_id = :rentalPointId")
    List<Bicycle> getAllAtRentalPoint(long rentalPointId);

    @Query("SELECT * FROM bicycle " +
            "WHERE rental_point_id = :rentalPointId " +
            "AND rented_by_client_id IS NULL " +
            "AND booked_by_client_id IS NULL")
    LiveData<List<Bicycle>> getAvailableAtRentalPointLiveData(long rentalPointId);

    @Query("SELECT * FROM bicycle " +
            "WHERE booked_by_client_id = :clientId " +
            "AND rented_by_client_id IS NULL")
    LiveData<List<Bicycle>> getBookedByClientLiveData(long clientId);

    @Query("SELECT * FROM bicycle " +
            "WHERE rented_by_client_id = :clientId")
    LiveData<List<Bicycle>> getRentedByClientLiveData(long clientId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Bicycle bicycle);

    @Update
    void update(Bicycle... bicycles);
}
