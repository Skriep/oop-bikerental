package by.skriep.bikerental.data.repositories;

import androidx.lifecycle.LiveData;

import java.util.List;

import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;
import by.skriep.bikerental.domain.entities.users.Client;

public interface BicyclesRepository {

    List<Bicycle> getAllBicycles();

    List<Bicycle> getAllBicycles(BikeRentalPoint rentalPoint);

    LiveData<List<Bicycle>> getAvailableBicyclesLiveData(BikeRentalPoint rentalPoint);

    LiveData<List<Bicycle>> getBookedByClientBicyclesLiveData(Client client);

    LiveData<List<Bicycle>> getRentedByClientBicyclesLiveData(Client client);

    void addNewBicycle(Bicycle bicycle);

    void updateBicycles(Bicycle... bicycles);
}
