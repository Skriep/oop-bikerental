package by.skriep.bikerental.data.repositories;

import androidx.lifecycle.LiveData;

import java.util.List;

import by.skriep.bikerental.domain.entities.trips.BikeTrip;
import by.skriep.bikerental.domain.entities.users.Client;

public interface BikeTripsRepository {

    List<BikeTrip> getAllBikeTrips();

    List<BikeTrip> getAllBikeTrips(Client client);

    LiveData<List<BikeTrip>> getAllBikeTripsLiveData(Client client);

    void addNewBikeTrip(BikeTrip bikeTrip);

    void updateBikeTrips(BikeTrip... bikeTrips);
}
