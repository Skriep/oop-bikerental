package by.skriep.bikerental.data.repositories;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import java.util.List;

import by.skriep.bikerental.data.BikeRentalDatabase;
import by.skriep.bikerental.data.relations.ClientWithBookedBicycles;
import by.skriep.bikerental.data.relations.ClientWithRentedBicycles;
import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;
import by.skriep.bikerental.domain.entities.trips.BikeTrip;
import by.skriep.bikerental.domain.entities.users.Administrator;
import by.skriep.bikerental.domain.entities.users.Client;
import by.skriep.bikerental.domain.entities.users.User;

public class BikeRentalRepository implements UsersRepository, BikeRentalPointsRepository,
        BicyclesRepository, BikeTripsRepository {

    private final BikeRentalDatabase database;

    public BikeRentalRepository(@NonNull BikeRentalDatabase database) {
        this.database = database;
    }

    @Nullable
    @Override
    public Client findClientByUsername(String username) {
        return database.clientDao().getByUsername(username);
    }

    @Override
    public ClientWithBookedBicycles getClientWithBookedBicyclesById(long clientId) {
        return database.clientDao().getWithBookedBicycles(clientId);
    }

    @Override
    public ClientWithRentedBicycles getClientWithRentedBicyclesById(long clientId) {
        return database.clientDao().getWithRentedBicycles(clientId);
    }

    @Override
    public void addNewClient(Client client) {
        long id = database.clientDao().insert(client);
        client.setId(id);
    }

    @Override
    public void updateClients(Client... clients) {
        database.clientDao().update(clients);
    }

    @Nullable
    @Override
    public Administrator findAdminByUsername(String username) {
        return database.administratorDao().getByUsername(username);
    }

    @Override
    public void addNewAdmin(Administrator admin) {
        long id = database.administratorDao().insert(admin);
        admin.setId(id);
    }

    @Override
    public void updateAdmins(Administrator... admins) {
        database.administratorDao().update(admins);
    }

    @Nullable
    @Override
    public User findUserByUsername(String username) {
        User user = database.clientDao().getByUsername(username);
        if (user != null) {
            return user;
        }
        return database.administratorDao().getByUsername(username);
    }

    @Override
    public void addNewUser(User user) {
        if (user instanceof Client) {
            addNewClient((Client) user);
        } else if (user instanceof Administrator) {
            addNewAdmin((Administrator) user);
        }
    }

    @Override
    public void updateUser(User user) {
        if (user instanceof Client) {
            updateClients((Client) user);
        } else if (user instanceof Administrator) {
            updateAdmins((Administrator) user);
        }
    }

    @Override
    public List<BikeRentalPoint> getAllBikeRentalPoints() {
        return database.bikeRentalPointDao().getAll();
    }

    @Override
    public List<BikeRentalPoint> getAllBikeRentalPointsInRegion(double minLatitude,
                                                                double minLongitude,
                                                                double maxLatitude,
                                                                double maxLongitude) {
        return database.bikeRentalPointDao()
                .getAllInRegion(minLatitude, minLongitude,maxLatitude, maxLongitude);
    }

    @Override
    public List<BikeRentalPoint> getBikeRentalPointsInRegion(double minLatitude,
                                                             double minLongitude,
                                                             double maxLatitude,
                                                             double maxLongitude,
                                                             long[] ignoredIds) {
        return database.bikeRentalPointDao()
                .getInRegion(minLatitude, minLongitude, maxLatitude, maxLongitude, ignoredIds);
    }

    @Override
    public void addNewBikeRentalPoint(BikeRentalPoint bikeRentalPoint) {
        long id = database.bikeRentalPointDao().insert(bikeRentalPoint);
        bikeRentalPoint.setId(id);
    }

    @Override
    public List<Bicycle> getAllBicycles() {
        return database.bicycleDao().getAll();
    }

    @Override
    public List<Bicycle> getAllBicycles(BikeRentalPoint rentalPoint) {
        return database.bicycleDao().getAllAtRentalPoint(rentalPoint.getId());
    }

    @Override
    public LiveData<List<Bicycle>> getAvailableBicyclesLiveData(BikeRentalPoint rentalPoint) {
        return database.bicycleDao().getAvailableAtRentalPointLiveData(rentalPoint.getId());
    }

    @Override
    public LiveData<List<Bicycle>> getBookedByClientBicyclesLiveData(Client client) {
        return database.bicycleDao().getBookedByClientLiveData(client.getId());
    }

    @Override
    public LiveData<List<Bicycle>> getRentedByClientBicyclesLiveData(Client client) {
        return database.bicycleDao().getRentedByClientLiveData(client.getId());
    }

    @Override
    public void addNewBicycle(Bicycle bicycle) {
        long id = database.bicycleDao().insert(bicycle);
        bicycle.setId(id);
    }

    @Override
    public void updateBicycles(Bicycle... bicycles) {
        database.bicycleDao().update(bicycles);
    }

    @Override
    public List<BikeTrip> getAllBikeTrips() {
        return database.bikeTripDao().getAll();
    }

    @Override
    public List<BikeTrip> getAllBikeTrips(Client client) {
        return database.bikeTripDao().getAll(client.getId());
    }

    @Override
    public LiveData<List<BikeTrip>> getAllBikeTripsLiveData(Client client) {
        return database.bikeTripDao().getAllLiveData(client.getId());
    }

    @Override
    public void addNewBikeTrip(BikeTrip bikeTrip) {
        long bikeTripId = database.bikeTripDao().insert(bikeTrip);
        bikeTrip.setId(bikeTripId);
    }

    @Override
    public void updateBikeTrips(BikeTrip... bikeTrips) {
        database.bikeTripDao().update(bikeTrips);
    }
}
