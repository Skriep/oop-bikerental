package by.skriep.bikerental.data.relations;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.users.Client;

public class ClientWithRentedBicycles {
    @Embedded
    public Client client;

    @Relation(
            parentColumn = "id",
            entityColumn = "rented_by_client_id"
    )
    public List<Bicycle> rentedBicycles;
}
