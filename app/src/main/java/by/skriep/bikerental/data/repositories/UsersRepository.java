package by.skriep.bikerental.data.repositories;

import androidx.annotation.Nullable;

import by.skriep.bikerental.domain.entities.users.User;

public interface UsersRepository extends AdminsRepository, ClientsRepository {

    @Nullable
    User findUserByUsername(String username);

    void addNewUser(User user);

    void updateUser(User user);
}
