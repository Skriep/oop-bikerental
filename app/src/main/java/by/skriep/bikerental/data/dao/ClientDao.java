package by.skriep.bikerental.data.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import by.skriep.bikerental.data.relations.ClientWithBookedBicycles;
import by.skriep.bikerental.data.relations.ClientWithRentedBicycles;
import by.skriep.bikerental.domain.entities.users.Client;

@Dao
public interface ClientDao {
    @Query("SELECT * FROM client")
    List<Client> getAll();

    @Query("SELECT * FROM client WHERE username = :username LIMIT 1")
    Client getByUsername(String username);

    @Transaction
    @Query("SELECT * FROM client WHERE id = :userId LIMIT 1")
    ClientWithRentedBicycles getWithRentedBicycles(long userId);

    @Transaction
    @Query("SELECT * FROM client")
    List<ClientWithBookedBicycles> getAllWithBookedBicycles();

    @Transaction
    @Query("SELECT * FROM client WHERE id = :userId LIMIT 1")
    ClientWithBookedBicycles getWithBookedBicycles(long userId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Client client);

    @Update
    void update(Client... clients);
}
