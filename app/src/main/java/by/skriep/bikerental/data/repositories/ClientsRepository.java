package by.skriep.bikerental.data.repositories;

import androidx.annotation.Nullable;

import by.skriep.bikerental.data.relations.ClientWithBookedBicycles;
import by.skriep.bikerental.data.relations.ClientWithRentedBicycles;
import by.skriep.bikerental.domain.entities.users.Client;

public interface ClientsRepository {

    @Nullable
    Client findClientByUsername(String username);

    ClientWithBookedBicycles getClientWithBookedBicyclesById(long clientId);

    ClientWithRentedBicycles getClientWithRentedBicyclesById(long clientId);

    void addNewClient(Client client);

    void updateClients(Client... clients);
}
