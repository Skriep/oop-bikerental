package by.skriep.bikerental.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import by.skriep.bikerental.domain.entities.users.Administrator;

@Dao
public interface AdministratorDao {
    @Query("SELECT * FROM administrator")
    List<Administrator> getAll();

    @Query("SELECT * FROM administrator WHERE username = :username LIMIT 1")
    Administrator getByUsername(String username);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Administrator administrator);

    @Update
    void update(Administrator... administrators);
}
