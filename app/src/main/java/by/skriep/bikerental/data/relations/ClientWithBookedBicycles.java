package by.skriep.bikerental.data.relations;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.users.Client;

public class ClientWithBookedBicycles {
    @Embedded
    public Client client;

    @Relation(
            parentColumn = "id",
            entityColumn = "booked_by_client_id"
    )
    public List<Bicycle> bookedBicycles;
}
