package by.skriep.bikerental.data.repositories;

import androidx.annotation.Nullable;

import by.skriep.bikerental.domain.entities.users.Administrator;

public interface AdminsRepository {

    @Nullable
    Administrator findAdminByUsername(String username);

    void addNewAdmin(Administrator admin);

    void updateAdmins(Administrator... admins);
}
