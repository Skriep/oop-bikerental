package by.skriep.bikerental.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;

@Dao
public interface BikeRentalPointDao {
    @Query("SELECT * FROM bikerentalpoint")
    List<BikeRentalPoint> getAll();

    @Query("SELECT * FROM bikerentalpoint " +
            "WHERE latitude >= :minLatitude AND longitude >= :minLongitude " +
            "AND latitude <= :maxLatitude AND longitude <= :maxLongitude")
    List<BikeRentalPoint> getAllInRegion(double minLatitude, double minLongitude,
                                         double maxLatitude, double maxLongitude);

    @Query("SELECT * FROM bikerentalpoint " +
            "WHERE latitude >= :minLatitude AND longitude >= :minLongitude " +
            "AND latitude <= :maxLatitude AND longitude <= :maxLongitude " +
            "AND id NOT IN (:ignoredIds)")
    List<BikeRentalPoint> getInRegion(double minLatitude, double minLongitude,
                                      double maxLatitude, double maxLongitude,
                                      long[] ignoredIds);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(BikeRentalPoint bikeRentalPoint);
}
