package by.skriep.bikerental.data.repositories;

import java.util.List;

import by.skriep.bikerental.data.relations.BikeRentalPointWithBicycles;
import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;

public interface BikeRentalPointsRepository {

    List<BikeRentalPoint> getAllBikeRentalPointsInRegion(double minLatitude, double minLongitude,
                                                         double maxLatitude, double maxLongitude);

    List<BikeRentalPoint> getBikeRentalPointsInRegion(double minLatitude, double minLongitude,
                                                      double maxLatitude, double maxLongitude,
                                                      long[] ignoredIds);

    List<BikeRentalPoint> getAllBikeRentalPoints();

    void addNewBikeRentalPoint(BikeRentalPoint bikeRentalPoint);
}
