package by.skriep.bikerental.data.relations;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;

public class BikeRentalPointWithBicycles {
    @Embedded
    public BikeRentalPoint bikeRentalPoint;

    @Relation(
            parentColumn = "id",
            entityColumn = "rental_point_id"
    )
    public List<Bicycle> bicycles;
}
