package by.skriep.bikerental.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import by.skriep.bikerental.domain.entities.trips.BikeTrip;

@Dao
public interface BikeTripDao {
    @Query("SELECT * FROM biketrip")
    List<BikeTrip> getAll();

    @Query("SELECT * FROM biketrip WHERE client_id = :clientId")
    List<BikeTrip> getAll(long clientId);

    @Query("SELECT * FROM biketrip WHERE client_id = :clientId")
    LiveData<List<BikeTrip>> getAllLiveData(long clientId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(BikeTrip bikeTrip);

    @Update
    void update(BikeTrip... bikeTrips);
}
