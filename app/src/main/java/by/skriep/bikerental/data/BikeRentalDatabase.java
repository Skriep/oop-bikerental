package by.skriep.bikerental.data;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import by.skriep.bikerental.data.dao.AdministratorDao;
import by.skriep.bikerental.data.dao.BicycleDao;
import by.skriep.bikerental.data.dao.BikeRentalPointDao;
import by.skriep.bikerental.data.dao.BikeTripDao;
import by.skriep.bikerental.data.dao.ClientDao;
import by.skriep.bikerental.domain.entities.bikes.Bicycle;
import by.skriep.bikerental.domain.entities.rental_points.BikeRentalPoint;
import by.skriep.bikerental.domain.entities.trips.BikeTrip;
import by.skriep.bikerental.domain.entities.users.Administrator;
import by.skriep.bikerental.domain.entities.users.Client;

@Database(entities = {Client.class, Administrator.class, BikeRentalPoint.class, Bicycle.class, BikeTrip.class},
        version = 7, exportSchema = false)
public abstract class BikeRentalDatabase extends RoomDatabase {
    public abstract ClientDao clientDao();
    public abstract AdministratorDao administratorDao();
    public abstract BikeRentalPointDao bikeRentalPointDao();
    public abstract BicycleDao bicycleDao();
    public abstract BikeTripDao bikeTripDao();
}
